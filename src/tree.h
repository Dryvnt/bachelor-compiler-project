#ifndef __tree_h
#define __tree_h

/* Catch-all for AST. Roughly translated directly from grammar. */

#include "symbol.h"
#include "type.h"

#include <stdbool.h>
#include <stdlib.h>

typedef struct VARDECL {
    int lineno;
    char *identifier;
    struct TYPE *type;
    struct VARDECL *next;
} VARDECL;

VARDECL *makeVARDECL(char *, struct TYPE *);

VARDECL *appendVARDECLlist(VARDECL *, VARDECL *);

typedef struct TYPE {
    enum {
        typenameK,
        inttypeK,
        booltypeK,
        arraytypeK,
        recordtypeK,
        implicittypeK, // created by type collection, holds no semantic value
    } kind;
    union {
        char *typenameT;
        struct TYPE *arraytypeT;
        VARDECL *recordtypeT;
    } val;
    P_type *type_val;
} TYPE;

TYPE *makeTYPEtypename(char *);
TYPE *makeTYPEinttype();
TYPE *makeTYPEbooltype();
TYPE *makeTYPEarraytype(TYPE *);
TYPE *makeTYPErecordtype(VARDECL *);
TYPE *makeTYPEimplicit(P_type *);

typedef struct HEAD {
    char *identifier;
    VARDECL *parameters;
    TYPE *returntype;
} HEAD;

HEAD *makeHEAD(char *, VARDECL *, TYPE *);

typedef struct BODY {
    struct DECL *declarations; // can't be const due to typecheck mutating
    struct STMT *statements;
} BODY;

BODY *makeBODY(struct DECL *, struct STMT *);

typedef struct TAIL { char *identifier; } TAIL;

TAIL *makeTAIL(char *);

typedef struct FUNC {
    HEAD *head;
    BODY *body;
    TAIL *tail;
    struct SymbolTable *symbols;
} FUNC;

FUNC *makeFUNC(HEAD *, BODY *, TAIL *);

// extracted for TreeVisitor
typedef struct TYPEDECL {
    char *identifier;
    TYPE *type;
} TYPEDECL;

typedef struct DECL {
    int lineno;
    enum { typedeclK, funcdeclK, vardeclK } kind;
    union {
        TYPEDECL *typedeclD;
        FUNC *funcdeclD;
        VARDECL *vardeclD;
    } val;
    struct DECL *next;
} DECL;

DECL *makeDECLtypedecl(char *, TYPE *);
DECL *makeDECLfuncdecl(FUNC *);
DECL *makeDECLvardecl(VARDECL *);

DECL *appendDECLlist(DECL *, DECL *);

typedef struct VAR {
    enum { identK, indexK, memberK } kind;
    union {
        char *identV;
        struct {
            struct VAR *var;
            struct EXP *index;
        } indexV;
        struct {
            struct VAR *var;
            char *member;
        } memberV;
    } val;
    P_type *type; // for typechecking.
} VAR;

VAR *makeVARident(char *);
VAR *makeVARindex(VAR *, struct EXP *);
VAR *makeVARmember(VAR *, char *);

typedef enum {
    plusO,
    minusO,
    multO,
    divO,
    eqO,
    neqO,
    gtO,
    ltO,
    geqO,
    leqO,
    andO,
    orO
} opType;

typedef enum {
    negationC, // !E
    absC,      // |E|
} expContainerType;

// exp and term
typedef struct EXP {
    int lineno;
    enum { opK, varK, callK, containerK, intconstK, boolconstK, nullK } kind;
    union {
        struct {
            opType op;
            struct EXP *a;
            struct EXP *b;
        } opE;
        VAR *varE;
        struct {
            char *identifier;
            struct EXP *head;
        } callE;
        struct {
            expContainerType container;
            struct EXP *e;
        } containerE;
        int intconstE;
        bool boolconstE;
    } val;
    P_type *type;     // for typechecking.
    struct EXP *next; // arg_list
} EXP;

EXP *makeEXPop(const opType, EXP *, EXP *);
EXP *makeEXPvar(VAR *);
EXP *makeEXPcall(char *, EXP *);
EXP *makeEXPcontainer(const expContainerType, EXP *);
EXP *makeEXPintconst(const int);
EXP *makeEXPboolconst(const bool);
EXP *makeEXPnull();

EXP *appendEXPlist(EXP *, EXP *);

typedef struct STMT {
    int lineno;
    enum {
        returnK,
        writeK,
        allocK,
        allocArrayK,
        assignK,
        ifThenK,
        ifThenElseK,
        whileK
    } kind;
    union {
        EXP *returnS; // can't be const because typecheck
        EXP *writeS;
        VAR *allocS;
        struct {
            VAR *var;
            EXP *length;
        } alloclengthS;
        struct {
            VAR *var;
            EXP *value;
        } assignS;
        struct {
            EXP *cond;
            struct STMT *action;
        } ifThenS;
        struct {
            EXP *cond;
            struct STMT *action;
            struct STMT *otherwise;
        } ifThenElseS;
        struct {
            EXP *cond;
            struct STMT *action;
        } whileS;
    } val;
    struct STMT *next;
} STMT;

STMT *makeSTMTreturn(EXP *);
STMT *makeSTMTwrite(EXP *);
STMT *makeSTMTallocate(VAR *);
STMT *makeSTMTallocatelength(VAR *, EXP *);
STMT *makeSTMTassignment(VAR *, EXP *);
STMT *makeSTMTifthen(EXP *, STMT *);
STMT *makeSTMTifthenelse(EXP *, STMT *, STMT *);
STMT *makeSTMTwhiledo(EXP *, STMT *);

STMT *appendSTMTlist(STMT *, STMT *);

#endif
