#ifndef __TYPEVISITOR_H
#define __TYPEVISITOR_H

#include "type.h"

struct TypeVisitor;

typedef int (*TypeVisitorFunc)(struct TypeVisitor *, P_type *);

typedef struct TypeVisitorFunctions {
    TypeVisitorFunc enter;
    TypeVisitorFunc exit;
} TypeVisitorFunctions;

typedef struct TypeVisitor {
    void *data;
    TypeVisitorFunc p_int;
    TypeVisitorFunc p_bool;
    TypeVisitorFunc p_pointer;
    TypeVisitorFunctions p_array;
    TypeVisitorFunctions p_record;
    TypeVisitorFunc p_unresolved;
    TypeVisitorFunc p_error;
    // Special: called before recursing into nametype pair
    struct {
        int (*enter)(struct TypeVisitor *, P_NameTypePair *);
        int (*exit)(struct TypeVisitor *, P_NameTypePair *);
    } p_nametypepair;
} TypeVisitor;

int accept_Type(TypeVisitor *, P_type *);

#endif
