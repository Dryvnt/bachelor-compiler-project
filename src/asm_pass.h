#ifndef __asm_pass_h
#define __asm_pass_h

#include "pir.h"

/* Monolithic function that runs all the secondary passes over the code
   Both necessary ones such as register allocation and label generation
   As well as less necessary ones such as an extremely simple optimizing
   pass */
void asm_pass(PIR_function *main);

#endif
