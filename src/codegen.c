#include "codegen.h"
#include "macros.h"
#include "memory.h"
#include "pir.h"
#include "symbol.h"
#include "tree.h"
#include "treevisitor.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

typedef struct code_var_pair {
    PIR_asm *code;
    PIR_var *var;
} code_var_pair;

/*
    Input: Amount of levels to go up (0 = stay here)
    Output: %rbp of foreign link in temp var
*/
static code_var_pair resolve_static_link(PIR_function *context, int level) {
    code_builder retcode = {0};
    code_var_pair out = {0};

    if (context->static_link_variables == NULL) {
        PIR_var_list *l = NEW(PIR_var_list);
        l->elem = make_PIR_var_register(R_RBP);
        l->next = NULL;
        context->static_link_variables = l;
    }

    PIR_var_list *scope_rbp = context->static_link_variables;

    // "Jump scopes" upwards until we find
    // nearest static link to next-up scope.
    for (int i = 0; i < level; i++) {
        if (scope_rbp->next == NULL) {
            PIR_var *ref_to_link =
                make_PIR_var_static_ref_const(scope_rbp->elem, -2);
            PIR_var *tmp = make_PIR_var_temporary(context);
            push_asm(&retcode, make_PIR_asm_double(LOAD, ref_to_link, tmp));
            char buf[256];
            sprintf(buf, "static link %i", level);
            PIR_asm_comment(retcode.tail, buf);

            PIR_var_list *l = NEW(PIR_var_list);
            l->elem = tmp;
            l->next = NULL;
            scope_rbp->next = l;
        }

        scope_rbp = scope_rbp->next;
    }

    out.var = scope_rbp->elem;

    out.code = retcode.head;
    return out;
}

static PIR_asm *codegen_conditional(PIR_function *context, PIR_var *a,
                                    PIR_var *b, PIR_asm_kind flow_kind,
                                    PIR_asm *action, PIR_asm *otherwise) {
    code_builder code = {0};
    PIR_asm *then_begin = make_PIR_asm_none(NOP);
    PIR_asm *end = make_PIR_asm_none(NOP);

    PIR_var *tmp = make_PIR_var_temporary(context);
    push_asm(&code, make_PIR_asm_double(LOAD, a, tmp));
    push_asm(&code, make_PIR_asm_double(CMP, b, tmp));

    push_asm(&code, make_PIR_asm_flow(flow_kind, then_begin));
    PIR_asm_comment(code.tail, "if");

    push_asm(&code, make_PIR_asm_none(NOP));
    PIR_asm_comment(code.tail, "else");
    push_asm(&code, otherwise);
    push_asm(&code, make_PIR_asm_flow(JUMP, end));

    push_asm(&code, then_begin);
    PIR_asm_comment(code.tail, "then");
    push_asm(&code, action);

    push_asm(&code, end);
    PIR_asm_comment(code.tail, "endif");
    return code.head;
}

// forward decl
static PIR_asm *codegen_stmt(STMT *s, PIR_function *context);
static code_var_pair codegen_exp(EXP *e, PIR_function *context);

PIR_asm_kind flow_kind_from_comparator_exp(EXP *e) {
    assert(e->kind == opK);
    switch (e->val.opE.op) {
    case eqO:
        return JUMP_EQ;
    case neqO:
        return JUMP_NEQ;
    case gtO:
        return JUMP_GT;
    case ltO:
        return JUMP_LT;
    case geqO:
        return JUMP_GEQ;
    case leqO:
        return JUMP_LEQ;
    default:
        assert(false);
    }
}

/* Common pattern. Extracting into function */
static PIR_asm *codegen_if_then_else(PIR_function *context, EXP *condition,
                                     STMT *action, STMT *otherwise) {
    code_builder code = {0};
    PIR_asm_kind flow_kind;
    PIR_var *a_var;
    PIR_var *b_var;

    bool flow_set = false;

    if (condition->kind == opK) {
        switch (condition->val.opE.op) {
        case eqO:
        case neqO:
        case gtO:
        case ltO:
        case geqO:
        case leqO: {
            code_var_pair a = codegen_exp(condition->val.opE.a, context);
            code_var_pair b = codegen_exp(condition->val.opE.b, context);
            push_asm(&code, a.code);
            push_asm(&code, b.code);

            flow_kind = flow_kind_from_comparator_exp(condition);
            flow_set = true;
            a_var = a.var;
            b_var = b.var;
        }
        default:
            break;
        }
    }

    if (!flow_set) {
        code_var_pair cond = codegen_exp(condition, context);
        push_asm(&code, cond.code);
        a_var = make_PIR_var_immediate(0);
        b_var = cond.var;
        flow_kind = JUMP_NEQ;
    }

    PIR_asm *action_code = codegen_stmt(action, context);
    PIR_asm *otherwise_code = codegen_stmt(otherwise, context);

    PIR_asm *conditional_code = codegen_conditional(
        context, a_var, b_var, flow_kind, action_code, otherwise_code);

    push_asm(&code, conditional_code);

    return code.head;
}

static code_var_pair codegen_exp(EXP *e, PIR_function *context) {
    code_builder retcode = {0};
    PIR_var *retvar;

    switch (e->kind) {
    case intconstK: {
        retvar = make_PIR_var_immediate(e->val.intconstE);
        break;
    }
    case boolconstK: {
        retvar = make_PIR_var_immediate(e->val.boolconstE);
        break;
    }
    case nullK: {
        retvar = make_PIR_var_immediate(0);
        break;
    }
    case varK: {
        VAR *v = e->val.varE;
        switch (v->kind) {
        case identK: {
            int level;
            Symbol *s =
                getSymbolLevel(context->symbols, v->val.identV, &level);
            assert(s != NULL);

            // Should be set before we begin generating asm
            assert(s->as_var != NULL);

            if (level == 0) {
                retvar = s->as_var;

            } else {
                PIR_var *static_var = s->as_var;

                code_var_pair foreign_rbp =
                    resolve_static_link(context, level);
                push_asm(&retcode, foreign_rbp.code);

                retvar = make_PIR_var_static_ref(foreign_rbp.var, static_var);
            }

            break;
        }
        case indexK:
        case memberK: {
            PIR_var *addr_var;
            PIR_var *offset = make_PIR_var_temporary(context);

            if (v->kind == indexK) {
                assert(v->val.indexV.var->type->kind == P_POINTER);
                EXP var_exp = {.kind = varK, .val.varE = v->val.indexV.var};
                code_var_pair var_pair = codegen_exp(&var_exp, context);
                code_var_pair exp_pair =
                    codegen_exp(v->val.indexV.index, context);
                push_asm(&retcode, var_pair.code);
                push_asm(&retcode, exp_pair.code);

                addr_var = var_pair.var;

                // really wish we had an intermediary form, so we could
                // have made this as an actual multiplication instead
                // of having this as a special-case multiplication that
                // uses other code than regular multiplication
                push_asm(&retcode,
                         make_PIR_asm_double(LOAD, exp_pair.var, offset));
                push_asm(&retcode,
                         make_PIR_asm_double(ADD, make_PIR_var_immediate(1),
                                             offset));

            } else if (v->kind == memberK) {
                assert(v->val.memberV.var->type->kind == P_POINTER);
                EXP var_exp = {.kind = varK, .val.varE = v->val.memberV.var};
                code_var_pair var_pair = codegen_exp(&var_exp, context);
                push_asm(&retcode, var_pair.code);
                addr_var = var_pair.var;

                P_type *inner = v->val.memberV.var->type->as_pointer.to;
                assert(inner->kind == P_RECORD);
                int num_member = 0;
                for (P_NameTypePair *it = inner->as_record.members; it != NULL;
                     it = it->next) {
                    if (strcmp(v->val.memberV.member, it->name) == 0) {
                        break;
                    }
                    num_member += 1;
                }

                push_asm(
                    &retcode,
                    make_PIR_asm_double(
                        LOAD, make_PIR_var_immediate(num_member), offset));

            } else {
                // this should never happen
                assert(false);
            }

            push_asm(&retcode, make_PIR_asm_double(
                                   IMUL, make_PIR_var_immediate(8), offset));

            PIR_var *offset_addr = make_PIR_var_temporary(context);
            push_asm(&retcode,
                     make_PIR_asm_double(LOAD, addr_var, offset_addr));
            push_asm(&retcode, make_PIR_asm_double(ADD, offset, offset_addr));

            retvar = make_PIR_var_static_ref_const(offset_addr, 0);

            break;
        }
        }
        break;
    }
    case opK: {
        code_var_pair a = codegen_exp(e->val.opE.a, context);
        code_var_pair b = codegen_exp(e->val.opE.b, context);

        push_asm(&retcode, a.code);
        push_asm(&retcode, b.code);

        switch (e->val.opE.op) {
        case plusO: {
            PIR_var *dest = make_PIR_var_temporary(context);
            push_asm(&retcode, make_PIR_asm_double(LOAD, a.var, dest));

            push_asm(&retcode, make_PIR_asm_double(ADD, b.var, dest));
            retvar = dest;
            break;
        }
        case minusO: {
            PIR_var *dest = make_PIR_var_temporary(context);
            push_asm(&retcode, make_PIR_asm_double(LOAD, a.var, dest));

            push_asm(&retcode, make_PIR_asm_double(SUB, b.var, dest));
            retvar = dest;
            break;
        }
        case multO: {
            PIR_var *dest = make_PIR_var_temporary(context);
            push_asm(&retcode, make_PIR_asm_double(LOAD, a.var, dest));

            push_asm(&retcode, make_PIR_asm_double(IMUL, b.var, dest));
            retvar = dest;
            break;
        }
        case andO: {
            PIR_var *dest = make_PIR_var_temporary(context);
            push_asm(&retcode, make_PIR_asm_double(LOAD, a.var, dest));

            push_asm(&retcode, make_PIR_asm_double(AND, b.var, dest));
            retvar = dest;
            break;
        }
        case orO: {
            PIR_var *dest = make_PIR_var_temporary(context);
            push_asm(&retcode, make_PIR_asm_double(LOAD, a.var, dest));

            push_asm(&retcode, make_PIR_asm_double(OR, b.var, dest));
            retvar = dest;
            break;
        }
        case divO: {
            PIR_var *divisor = make_PIR_var_temporary(context);
            PIR_var *dest = make_PIR_var_temporary(context);

            push_asm(&retcode, make_PIR_asm_double(LOAD, b.var, divisor));
            push_asm(&retcode, make_PIR_asm_double(
                                   LOAD, a.var, make_PIR_var_register(R_RAX)));
            push_asm(&retcode, make_PIR_asm_single(IDIV, divisor));
            push_asm(&retcode, make_PIR_asm_double(
                                   LOAD, make_PIR_var_register(R_RAX), dest));
            retvar = dest;
            break;
        }
        case eqO:
        case neqO:
        case gtO:
        case ltO:
        case geqO:
        case leqO: {
            PIR_var *dest = make_PIR_var_temporary(context);
            PIR_asm *action =
                make_PIR_asm_double(LOAD, make_PIR_var_immediate(1), dest);
            PIR_asm *otherwise =
                make_PIR_asm_double(LOAD, make_PIR_var_immediate(0), dest);

            PIR_asm_kind flow_kind = flow_kind_from_comparator_exp(e);

            push_asm(&retcode,
                     codegen_conditional(context, a.var, b.var, flow_kind,
                                         action, otherwise));
            retvar = dest;

            break;
        }
        }
        break;
    }
    case callK: {
        /* parameter gathering and generation */
        // TODO: args in registers
        PIR_var_list *param_list = NULL;
        for (EXP *it = e->val.callE.head; it != NULL; it = it->next) {
            code_var_pair p = codegen_exp(it, context);
            push_asm(&retcode, p.code);
            PIR_var_list *l = NEW(PIR_var_list);
            l->elem = p.var;

            // Reverse order of EXP-list
            l->next = param_list;
            param_list = l;
        }

        /* Create static link */
        int level;
        Symbol *s =
            getSymbolLevel(context->symbols, e->val.callE.identifier, &level);
        assert(s != NULL);

        code_var_pair link = resolve_static_link(context, level);
        push_asm(&retcode, link.code);

        /* code for saving registers */

        PIR_asm *call =
            make_PIR_asm_unresolved_call(cloneString(e->val.callE.identifier));
        PIR_asm *post = make_PIR_asm_none(POST_CALL);
        PIR_asm *pre = make_PIR_asm_pre_call(call, post);

        push_asm(&retcode, pre);

        /* parameter pushing */
        int num_params = 0;
        for (PIR_var_list *it = param_list; it != NULL; it = it->next) {
            push_asm(&retcode, make_PIR_asm_single(PUSH, it->elem));
            num_params += 1;
        }

        /* push static link */
        push_asm(&retcode, make_PIR_asm_single(PUSH, link.var));
        PIR_asm_comment(retcode.tail, "Push static link");

        push_asm(&retcode, call);

        // +1 because of static link
        PIR_var *pop_amount = make_PIR_var_immediate(8 * (num_params + 1));
        push_asm(&retcode, make_PIR_asm_double(ADD, pop_amount,
                                               make_PIR_var_register(R_RSP)));
        PIR_asm_comment(retcode.tail, "pop params, link");

        push_asm(&retcode, post);

        retvar = make_PIR_var_temporary(context);
        push_asm(&retcode, make_PIR_asm_double(
                               LOAD, make_PIR_var_register(R_RAX), retvar));

        break;
    }
    case containerK: {
        code_var_pair inner = codegen_exp(e->val.containerE.e, context);
        push_asm(&retcode, inner.code);

        switch (e->val.containerE.container) {
        case negationC: {
            // should always be true if we pass typechecking
            assert(e->val.containerE.e->type->kind == P_BOOL);

            PIR_var *tmp = make_PIR_var_temporary(context);

            push_asm(&retcode, make_PIR_asm_double(LOAD, inner.var, tmp));
            push_asm(&retcode,
                     make_PIR_asm_double(XOR, make_PIR_var_immediate(1), tmp));

            retvar = tmp;

            break;
        }
        case absC: {
            EXP *sub_e = e->val.containerE.e;
            code_var_pair sub_inner = codegen_exp(sub_e, context);
            push_asm(&retcode, sub_inner.code);

            // really wish we had an intermediary language generated at
            // typechecking. Generating AST nodes during code generation...
            if (sub_e->type->kind == P_INT) {
                retvar = make_PIR_var_temporary(context);
                PIR_var *tmp = make_PIR_var_temporary(context);

                code_builder negation = {0};
                push_asm(&negation,
                         make_PIR_asm_double(LOAD, sub_inner.var, tmp));
                push_asm(&negation, make_PIR_asm_single(NEG, tmp));
                push_asm(&negation, make_PIR_asm_double(LOAD, tmp, retvar));

                push_asm(
                    &retcode,
                    codegen_conditional(
                        context, sub_inner.var, make_PIR_var_immediate(0),
                        JUMP_LT, negation.head,
                        make_PIR_asm_double(LOAD, sub_inner.var, retvar)));

            } else if (sub_e->type->kind == P_POINTER) {
                assert(sub_e->type->as_pointer.to->kind == P_ARRAY);
                PIR_var *tmp = make_PIR_var_temporary(context);
                push_asm(&retcode,
                         make_PIR_asm_double(LOAD, sub_inner.var, tmp));
                retvar = make_PIR_var_temporary(context);
                push_asm(
                    &retcode,
                    make_PIR_asm_double(
                        LOAD, make_PIR_var_static_ref_const(tmp, 0), retvar));
            } else {
                // should never happen
                assert(false);
            }
            break;
        }
        }
        break;
    }
    }

    assert(retvar != NULL); // retvar should always be set before return
    code_var_pair ret = {.code = retcode.head, .var = retvar};
    return ret;
}

static PIR_asm *codegen_stmt(STMT *s, PIR_function *context) {
    code_builder code = {0};

    if (s == NULL) {
        return make_PIR_asm_none(NOP);
    }

    switch (s->kind) {
    case writeK: {
        code_var_pair p = codegen_exp(s->val.writeS, context);

        push_asm(&code, p.code);
        push_asm(&code, make_PIR_asm_double(LOAD, p.var,
                                            make_PIR_var_register(R_RDI)));

        PIR_asm *call;
        PIR_asm *post = make_PIR_asm_none(POST_CALL);
        if (s->val.writeS->type->kind == P_INT) {
            call = make_PIR_asm_none(PRINT_INT_CALL);
        } else if (s->val.writeS->type->kind == P_BOOL) {
            call = make_PIR_asm_none(PRINT_BOOL_CALL);
        } else {
            // should never happen after typechecking
            assert(false);
        }

        PIR_asm *pre = make_PIR_asm_pre_call(call, post);
        push_asm(&code, pre);
        push_asm(&code, call);
        push_asm(&code, post);

        break;
    }
    case returnK: {
        code_var_pair p = codegen_exp(s->val.writeS, context);
        push_asm(&code, p.code);
        push_asm(&code, make_PIR_asm_double(LOAD, p.var,
                                            make_PIR_var_register(R_RAX)));
        push_asm(&code, make_PIR_asm_none(UNRESOLVED_RETURN));
        break;
    }
    case assignK: {
        EXP var_exp = {.kind = varK, .val.varE = s->val.assignS.var};
        code_var_pair var_pair = codegen_exp(&var_exp, context);
        code_var_pair exp_pair = codegen_exp(s->val.assignS.value, context);
        push_asm(&code, var_pair.code);
        push_asm(&code, exp_pair.code);
        push_asm(&code, make_PIR_asm_double(LOAD, exp_pair.var, var_pair.var));

        break;
    }
    case whileK: {
        push_asm(&code, codegen_if_then_else(context, s->val.whileS.cond,
                                             s->val.whileS.action, NULL));

        PIR_asm *last_of_action = code.head;
        while (last_of_action->next != code.tail) {
            last_of_action = last_of_action->next;
        }

        PIR_asm *jump_to_start = make_PIR_asm_flow(JUMP, code.head);
        // manual insert
        jump_to_start->next = last_of_action->next;
        last_of_action->next = jump_to_start;
        break;
    }
    case ifThenK: {
        push_asm(&code, codegen_if_then_else(context, s->val.ifThenS.cond,
                                             s->val.ifThenS.action, NULL));
        break;
    }
    case ifThenElseK: {
        push_asm(&code, codegen_if_then_else(context, s->val.ifThenElseS.cond,
                                             s->val.ifThenElseS.action,
                                             s->val.ifThenElseS.otherwise));
        break;
    }
    case allocK:
    case allocArrayK: {
        PIR_var *alloc_length = make_PIR_var_temporary(context);
        PIR_var *mem_pointer = make_PIR_var_temporary(context);
        code_var_pair var_pair;
        code_var_pair length_pair; // for arrays

        if (s->kind == allocK) {
            VAR *pointer = s->val.allocS;
            assert(pointer->type->kind == P_POINTER);

            EXP var_exp = {.kind = varK, .val.varE = pointer};
            var_pair = codegen_exp(&var_exp, context);

            P_type *inner = pointer->type->as_pointer.to;
            assert(inner->kind == P_RECORD);

            int num_members = 0;
            for (P_NameTypePair *it = inner->as_record.members; it != NULL;
                 it = it->next) {
                num_members += 1;
            }

            push_asm(&code, make_PIR_asm_double(
                                LOAD, make_PIR_var_immediate(num_members),
                                alloc_length));
        } else if (s->kind == allocArrayK) {
            length_pair = codegen_exp(s->val.alloclengthS.length, context);
            push_asm(&code, length_pair.code);

            push_asm(&code,
                     make_PIR_asm_double(LOAD, make_PIR_var_immediate(1),
                                         alloc_length));
            push_asm(&code,
                     make_PIR_asm_double(ADD, length_pair.var, alloc_length));
            EXP var_exp = {.kind = varK, .val.varE = s->val.alloclengthS.var};
            var_pair = codegen_exp(&var_exp, context);

        } else {
            assert(false);
        }

        PIR_asm *call = make_PIR_asm_none(ALLOC_CALL);
        PIR_asm *post = make_PIR_asm_none(POST_CALL);
        PIR_asm *pre = make_PIR_asm_pre_call(call, post);

        push_asm(&code, make_PIR_asm_double(LOAD, alloc_length,
                                            make_PIR_var_register(R_RDI)));

        push_asm(&code, pre);
        push_asm(&code, call);
        push_asm(&code, post);

        if (s->kind == allocArrayK) {
            push_asm(&code, make_PIR_asm_double(
                                LOAD, length_pair.var,
                                make_PIR_var_static_ref_const(
                                    make_PIR_var_register(R_RAX), 0)));
        }

        push_asm(&code, make_PIR_asm_double(LOAD, make_PIR_var_register(R_RAX),
                                            mem_pointer));

        push_asm(&code, var_pair.code);
        push_asm(&code, make_PIR_asm_double(LOAD, mem_pointer, var_pair.var));

        break;
    }
    }
    assert(code.head != NULL);
    if (s->next != NULL) {
        push_asm(&code, codegen_stmt(s->next, context));
    }
    return code.head;
}

static PIR_function *codegen_func(FUNC *f, PIR_function *parent) {
    PIR_function *current = NEW(PIR_function);
    current->parent = parent;
    current->symbols = f->symbols;
    current->num_temporaries = 0;

    if (current->parent == NULL) {
        current->name = cloneString("main");
    } else {
        char *tmp = cloneString(current->parent->name);
        tmp = appendString(tmp, ".");
        tmp = appendString(tmp, f->head->identifier);
        current->name = tmp;
    }

    /* Create temporaries for local variables */
    for (DECL *it = f->body->declarations; it != NULL; it = it->next) {
        if (it->kind == vardeclK) {
            for (VARDECL *dit = it->val.vardeclD; dit != NULL;
                 dit = dit->next) {
                int level;
                Symbol *s =
                    getSymbolLevel(current->symbols, dit->identifier, &level);
                assert(s != NULL);
                assert(level == 0);
                s->as_var = make_PIR_var_temporary(current);
            }
        }
    }

    /* Create PIR_vars for parameters. Not temporaries */
    int arg_num = 1;
    for (VARDECL *it = f->head->parameters; it != NULL; it = it->next) {
        PIR_var *placement;
        int arg_pos = get_parameter_placement(arg_num);
        arg_num += 1;
        if (arg_pos > 0) {
            placement = make_PIR_var_register(arg_pos);
        } else {
            placement = make_PIR_var_static(arg_pos);
        }

        int level;
        Symbol *s = getSymbolLevel(current->symbols, it->identifier, &level);
        assert(s != NULL);
        assert(level == 0);
        s->as_var = placement;
    }

    /* Generate code for sub-functions */
    for (DECL *it = f->body->declarations; it != NULL; it = it->next) {
        if (it->kind == funcdeclK) {
            PIR_func_list *l = NEW(PIR_func_list);
            l->elem = codegen_func(it->val.funcdeclD, current);
            l->next = current->sub_functions;
            current->sub_functions = l;

            int level;
            const char *name = it->val.funcdeclD->head->identifier;
            Symbol *s = getSymbolLevel(current->symbols, name, &level);
            assert(s != NULL);
            assert(level == 0);
            s->as_func = l->elem;
        }
    }

    /* Generate code for statements */
    code_builder inner_code = {0};

    // codegen_stmt handles itself being a list
    push_asm(&inner_code, codegen_stmt(f->body->statements, current));

    current->inner_code = inner_code.head;

    return current;
}

static void resolve_calls(PIR_function *f) {
    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        switch (it->kind) {
        case UNRESOLVED_CALL: {
            Symbol *s = getSymbol(f->symbols, it->as_unresolved_call);
            assert(s != NULL);
            Free(it->as_unresolved_call);

            it->kind = CALL;
            it->as_call = s->as_func;
        }
        default:
            break;
        }
    }

    for (PIR_func_list *it = f->sub_functions; it != NULL; it = it->next) {
        resolve_calls(it->elem);
    }
}

/* First pass */
PIR_function *codegen(FUNC *main) {
    // TODO: visitor pattern?

    PIR_LOG("Doing first pass...\n");
    PIR_function *_main = codegen_func(main, NULL);
    PIR_LOG("Resolving calls...\n");
    resolve_calls(_main);

    return _main;
}
