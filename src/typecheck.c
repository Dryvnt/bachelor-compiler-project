#include "memory.h"
#include "symbol.h"
#include "tree.h"
#include "treevisitor.h"
#include "type.h"
#include "typevisitor.h"

#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// early return. Try to do the thing, and propagate error upwards if it fails
#define TRY(e)            \
    {                     \
        if (!(e))         \
            return false; \
    }

extern bool arg_typechecklog;

void TYPECHECK_LOG(const char *fmt, ...) {
    if (arg_typechecklog) {
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
    }
}

static void dump_types(FUNC *f) {
    TYPECHECK_LOG("Dumping symbols for %s:\n", f->head->identifier);
    SymbolTable *symbols = f->symbols;
    for (NameList *it = symbols->names_it; it != NULL; it = it->next) {
        Symbol *s = getSymbol(symbols, it->name);

        TYPECHECK_LOG("\t");
        SYMBOL_LOG_SYMBOL(s);
        TYPECHECK_LOG("\n");
    }

    for (const DECL *it = f->body->declarations; it != NULL; it = it->next) {
        if (it->kind == funcdeclK) {
            dump_types(it->val.funcdeclD);
        }
    }
}

struct _collect_data {
    SymbolTable *symbols;
    int lineno;
};

static P_type *_type_from_tree(struct _collect_data *data, char *name,
                               const TYPE *t) {
    P_type *p = NEW(P_type);

    switch (t->kind) {
    case inttypeK: {
        p->kind = P_INT;
        break;
    }
    case booltypeK: {
        p->kind = P_BOOL;
        break;
    }
    case arraytypeK:
    case recordtypeK: {
        /* Create anonymous type of array/record,
           which we then become a pointer to*/
        P_type *inner = NEW(P_type);

        char *inner_name = cloneString("$");
        inner_name = appendString(inner_name, name);

        if (t->kind == arraytypeK) {
            inner->kind = P_ARRAY;

            char *index_name = cloneString(name);
            index_name = appendString(index_name, "[]");

            inner->as_array.of_type =
                _type_from_tree(data, index_name, t->val.arraytypeT);

            Free(index_name);
        } else if (t->kind == recordtypeK) {
            inner->kind = P_RECORD;

            for (VARDECL *it = t->val.recordtypeT; it != NULL; it = it->next) {
                char *member_name = cloneString(name);
                member_name = appendString(member_name, ".");
                member_name = appendString(member_name, it->identifier);

                P_NameTypePair *pair = NEW(P_NameTypePair);
                pair->name = cloneString(it->identifier);
                pair->type = _type_from_tree(data, member_name, it->type);

                // insert in front of list
                pair->next = inner->as_record.members;
                inner->as_record.members = pair;

                Free(member_name);
            }
        } else {
            assert(false); // sanity check
        }

        putSymbol(data->symbols, inner_name, P_TYPE, inner, data->lineno);

        p->kind = P_POINTER;
        p->as_pointer.name = inner_name;
        p->as_pointer.to = inner;
        break;
    }
    case typenameK: {
        p->kind = P_UNRESOLVED;
        p->as_unresolved.name = cloneString(t->val.typenameT);
        break;
    }
    default:
        assert(false);
    }

    return p;
}

static void _collect_type(struct _collect_data *data, char *name,
                          SymbolKind kind, TYPE *t) {
    t->type_val = _type_from_tree(data, name, t);

    // Only happens for _main
    if (data->symbols == NULL) {
        return;
    }

    putSymbol(data->symbols, name, kind, t->type_val, data->lineno);
}

static int _collect_func_enter(TreeVisitor *v, void *n) {
    struct _collect_data *data = v->data;
    FUNC *f = n;
    char *identifier = f->head->identifier;

    SymbolTable *parent = data->symbols;
    assert(f->symbols == NULL);

    TYPECHECK_LOG("Collecting symbols for %s...\n", identifier);

    // collect self
    // if parent == NULL, we won't change any symbol table
    // but this will collect the type of main, for later use
    _collect_type(data, f->head->identifier, P_FUNC, f->head->returntype);

    // set up own symbol table
    if (parent == NULL) {
        f->symbols = initSymbolTable();
    } else {
        f->symbols = scopeSymbolTable(parent);
    }

    // set up tree recursion
    data->symbols = f->symbols;

    return 0;
}

static int _collect_func_exit(TreeVisitor *v, void *n) {
    struct _collect_data *data = v->data;
    FUNC *f = n;

    // dismantle recursion - go up a level
    f->symbols = data->symbols;
    data->symbols = f->symbols->parent;

    /* collect args for function */
    SymbolTable *parent = f->symbols->parent;
    if (parent != NULL) {
        char *identifier = f->head->identifier;

        Symbol *s = getSymbol(f->symbols->parent, identifier);
        assert(s != NULL);

        P_NameTypePair *last = s->params;

        assert(last == NULL); // we should only collect args once

        for (VARDECL *it = f->head->parameters; it != NULL; it = it->next) {
            P_NameTypePair *new = NEW(P_NameTypePair);

            new->name = cloneString(it->identifier);
            new->type = it->type->type_val;

            if (last == NULL) {
                s->params = new;
            } else {
                last->next = new;
            }
            last = new;
        }
    }

    /* modify declarations to include implicit types */
    for (NameList *it = f->symbols->names_it; it != NULL; it = it->next) {
        // if type is implicit
        if (it->name[0] == '$') {
            Symbol *s = getSymbol(f->symbols, it->name);
            assert(s != NULL);
            assert(s->kind == P_TYPE);

            // create the implicit AST node
            TYPE *implicit_type = makeTYPEimplicit(s->type);
            DECL *implicit_decl =
                makeDECLtypedecl(cloneString(s->name), implicit_type);

            // insert it into the tree
            implicit_decl->next = f->body->declarations;
            f->body->declarations = implicit_decl;
        }
    }

    return 0;
}

static int _collect_decl(TreeVisitor *v, void *n) {
    struct _collect_data *data = v->data;
    DECL *d = n;
    data->lineno = d->lineno;

    return 0;
}

static int _collect_typedecl(TreeVisitor *v, void *n) {
    struct _collect_data *data = v->data;
    TYPEDECL *t = n;

    _collect_type(data, t->identifier, P_TYPE, t->type);
    return 0;
}

static int _collect_vardecl(TreeVisitor *v, void *n) {
    struct _collect_data *data = v->data;
    VARDECL *vd = n;
    assert(data->lineno = vd->lineno); // lol

    _collect_type(data, vd->identifier, P_VAR, vd->type);
    return 0;
}

static bool collect_symbols(FUNC *f) {
    struct _collect_data data = {.symbols = NULL, .lineno = 0};
    TreeVisitor visitor = {
        .data = &data,
        .func = {.enter = _collect_func_enter, .exit = _collect_func_exit},
        .decl = {.enter = _collect_decl},
        .typedecl = _collect_typedecl,
        .vardecl = _collect_vardecl,
    };

    return accept_TreeNode(&visitor, TV_FUNC, f);
}

struct _resolve_data {
    SymbolTable *symbols;
    bool *dirty;
    int *num_errors;
};

static int _resolve_unresolved(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_UNRESOLVED);
    struct _resolve_data *data = v->data;

    Symbol *s = getSymbol(data->symbols, t->as_unresolved.name);

    if (s == NULL) {
        fprintf(stderr, "ERR: Type '%s' does not exist\n",
                t->as_unresolved.name);
        t->kind = P_ERROR;
        *data->dirty = true;
        *data->num_errors += 1;
        return 0;
    }

    if (s->kind != P_TYPE) {
        fprintf(stderr, "ERR: Symbol '%s' is not a type\n",
                t->as_unresolved.name);
        t->kind = P_ERROR;
        *data->dirty = true;
        *data->num_errors += 1;
        return 0;
    }

    char *former = P_type_str(t);

    bool changed = false;
    switch (s->type->kind) {
    case P_INT:
    case P_BOOL:
    case P_POINTER:
        /* shallow copy */
        *t = *s->type;

        /* deeper copy if we have to */
        if (t->kind == P_POINTER) {
            t->as_pointer.name = cloneString(t->as_pointer.name);
        }
        changed = true;
        break;
    case P_ERROR: {
        t->kind = P_POINTER;
        t->as_pointer.name = cloneString(s->name);
        t->as_pointer.to = s->type;
        changed = true;
        break;
    }
    case P_UNRESOLVED:
        // do not copy unresolved type
        break;
    case P_ARRAY:
    case P_RECORD:
        assert(false); // should never happen after collection
    }

    if (changed) {
        *data->dirty = true;
        char *new = P_type_str(t);
        TYPECHECK_LOG("\t%s -> %s\n", former, new);
        Free(new);
    }
    Free(former);

    return 0;
}

static int _resolve_func(TreeVisitor *v, void *n) {
    int *num_errors = v->data;
    FUNC *f = n;

    bool dirty;

    do {
        dirty = false;
        TYPECHECK_LOG("Doing type resolution pass on %s...\n",
                      f->head->identifier);

        for (NameList *it = f->symbols->names_it; it != NULL; it = it->next) {
            struct _resolve_data data = {
                .symbols = f->symbols,
                .dirty = &dirty,
                .num_errors = num_errors,
            };

            TypeVisitor resolve_visitor = {
                .data = &data, .p_unresolved = _resolve_unresolved,
            };

            Symbol *s = getSymbol(f->symbols, it->name);
            int old_errors = *num_errors;

            accept_Type(&resolve_visitor, s->type);

            if (old_errors != *num_errors) {
                char *type_str = P_type_str(s->type);
                fprintf(stderr, "Error on type '%s': '%s'\n", it->name,
                        type_str);
                Free(type_str);
            }
        }
    } while (dirty);

    return 0;
}

static int resolve_types(FUNC *f) {
    int num_errors = 0;

    TreeVisitor resolve_visitor = {
        .data = &num_errors, .func = {.enter = _resolve_func},
    };

    accept_TreeNode(&resolve_visitor, TV_FUNC, f);

    TYPECHECK_LOG("Type resolution complete\n");

    return num_errors;
}

static int _ensure_resolved_unresolved(TypeVisitor *v, P_type *n) {
    int *num = v->data;
    assert(n->kind == P_UNRESOLVED);

    char *str = P_type_str(n);
    fprintf(stderr, "ERR: Unresolved type '%s'\n", str);
    Free(str);

    *num += 1;

    return 0;
}

static int _ensure_resolved_func(TreeVisitor *v, void *n) {
    int *num = v->data;
    FUNC *node = n;

    TypeVisitor resolved_visitor = {
        .data = num, .p_unresolved = _ensure_resolved_unresolved};

    for (NameList *it = node->symbols->names_it; it != NULL; it = it->next) {
        Symbol *s = getSymbol(node->symbols, it->name);
        assert(s != NULL);

        accept_Type(&resolved_visitor, s->type);
    }

    return 0;
}

static int ensure_resolved(FUNC *f) {
    TYPECHECK_LOG("Ensuring all types are fully resolved...\n");

    int num_unresolved = 0;

    TreeVisitor resolved_visitor = {.data = &num_unresolved,
                                    .func = {.enter = _ensure_resolved_func}};
    accept_TreeNode(&resolved_visitor, TV_FUNC, f);
    return -num_unresolved;
}

struct _check_data {
    struct _check_data *parent;
    P_type *return_type;
    SymbolTable *symbols;
};

static int _check_func_enter(TreeVisitor *v, void *n) {
    struct _check_data *data = v->data;
    FUNC *node = n;

    TYPECHECK_LOG("Checking function %s\n", node->head->identifier);

    // create new checking 'scope'
    struct _check_data *new_data = NEW(struct _check_data);
    new_data->parent = data;
    new_data->return_type = node->head->returntype->type_val;
    new_data->symbols = node->symbols;

    // add scope to stack
    v->data = new_data;

    return 0;
}

static int _check_func_exit(TreeVisitor *v, void *n) {
    struct _check_data *data = v->data;
    (void)n;

    // pop scope from stack
    struct _check_data *parent = data->parent;
    v->data = parent;
    Free(data);

    return 0;
}

/* purely logging */
static int _check_stmt_enter(TreeVisitor *v, void *n) {
    (void)v;
    STMT *node = n;
    TYPECHECK_LOG("Checking STMT line %i (%p)\n", node->lineno, (void *)node);
    return 0;
}

/* actual checking happens here */
static int _check_stmt_exit(TreeVisitor *v, void *n) {
    struct _check_data *data = v->data;
    STMT *node = n;

    switch (node->kind) {
    case returnK: {
        EXP *e = node->val.returnS;
        if (!P_type_eq(e->type, data->return_type)) {
            char *exp_str = P_type_str(e->type);
            char *ret_str = P_type_str(data->return_type);
            fprintf(stderr,
                    "ERR: <%i> Improper return type.\n"
                    "\tExpected type '%s', got %s\n",
                    node->lineno, ret_str, exp_str);
            Free(ret_str);
            Free(exp_str);
            return -1;
        }

        break;
    }
    case writeK: {
        EXP *e = node->val.writeS;

        if (e->type->kind != P_INT && e->type->kind != P_BOOL) {
            char *exp_str = P_type_str(e->type);
            fprintf(stderr, "ERR: <%i> Cannot write type '%s'\n", node->lineno,
                    exp_str);
            Free(exp_str);
            return -1;
        }

        break;
    }
    case assignK: {
        VAR *var = node->val.assignS.var;

        if (var->kind == identK) {
            char *identifier = var->val.identV;

            Symbol *s = getSymbol(data->symbols, identifier);

            if (s->kind != P_VAR) {
                fprintf(
                    stderr,
                    "ERR: <%i> Cannot assign to non-variable symbol '%s'\n",
                    node->lineno, identifier);
                return -1;
            }
        }

        P_type *var_type = var->type;
        P_type *value_type = node->val.assignS.value->type;

        if (!P_type_eq(var_type, value_type)) {
            char *var_str = P_type_str(var_type);
            char *value_str = P_type_str(value_type);
            fprintf(stderr,
                    "ERR: <%i> Cannot assign type '%s' "
                    "to variable of type '%s'\n",
                    node->lineno, value_str, var_str);
            Free(var_str);
            Free(value_str);
            return -1;
        }
        break;
    }
    case ifThenK:
    case ifThenElseK:
    case whileK: {
        /* if-then, while depends on union layout */
        EXP *cond = node->val.ifThenElseS.cond;

        if (cond->type->kind != P_BOOL) {
            char *type_str = P_type_str(cond->type);
            fprintf(stderr,
                    "ERR: <%i> Cannot use non-bool type '%s' in "
                    "conditional statements\n",
                    node->lineno, type_str);
            Free(type_str);
            return -1;
        }
        break;
    }
    case allocK:
    case allocArrayK: {
        VAR *var;
        if (node->kind == allocK) {
            var = node->val.allocS;
        } else if (node->kind == allocArrayK) {
            var = node->val.alloclengthS.var;
        }

        if (var->type->kind != P_POINTER) {
            char *type_str = P_type_str(var->type);
            fprintf(stderr, "ERR: <%i> Cannot alloc non-pointer type '%s'\n",
                    node->lineno, type_str);
            Free(type_str);
            return -1;
        }

        P_type *inner = var->type->as_pointer.to;

        if (node->kind == allocK) {
            if (inner->kind != P_RECORD) {
                char *type_str = P_type_str(inner);
                fprintf(stderr,
                        "ERR: <%i> Cannot alloc "
                        "non-record type '%s'\n",
                        node->lineno, type_str);
                Free(type_str);
                return -1;
            }
        } else if (node->kind == allocArrayK) {
            if (inner->kind != P_ARRAY) {
                char *type_str = P_type_str(inner);
                fprintf(stderr,
                        "ERR: <%i> Cannot alloc-of-length "
                        "non-array type '%s'\n",
                        node->lineno, type_str);
                Free(type_str);
                return -1;
            }

            EXP *exp = node->val.alloclengthS.length;

            if (exp->type->kind != P_INT) {
                char *type_str = P_type_str(exp->type);
                fprintf(stderr,
                        "ERR: <%i> Cannot alloc-of-length of non-int "
                        "length. Got type '%s'\n",
                        node->lineno, type_str);
                Free(type_str);
                return -1;
            }
        }

        break;
    }
    default:
        assert(false);
    }

    return 0;
}

static char *_op_str(opType op) {
    switch (op) {
    case plusO:
        return cloneString("+");
    case minusO:
        return cloneString("-");
    case multO:
        return cloneString("*");
    case divO:
        return cloneString("/");
    case eqO:
        return cloneString("==");
    case neqO:
        return cloneString("!=");
    case gtO:
        return cloneString(">");
    case ltO:
        return cloneString("<");
    case geqO:
        return cloneString(">=");
    case leqO:
        return cloneString("<=");
    case andO:
        return cloneString("&&");
    case orO:
        return cloneString("||");
    }
    assert(false); // unreachable
}

static P_type *_new_int() {
    P_type *tmp = NEW(P_type);
    tmp->kind = P_INT;
    return tmp;
}

static P_type *_new_bool() {
    P_type *tmp = NEW(P_type);
    tmp->kind = P_BOOL;
    return tmp;
}

static P_type *_new_null_pointer() {
    P_type *tmp = NEW(P_type);
    tmp->kind = P_POINTER;
    tmp->as_pointer.to = NULL;
    return tmp;
}

static int _check_op(EXP *e) {
    assert(e->kind == opK);

    EXP *a = e->val.opE.a;
    EXP *b = e->val.opE.b;

    switch (e->val.opE.op) {
    case plusO:
    case minusO:
    case multO:
    case divO:
        if (P_type_eq(a->type, b->type) && a->type->kind == P_INT) {
            e->type = _new_int();
            return 0;
        }
        break;
    case eqO:
    case neqO:
        if (P_type_eq(a->type, b->type)
            && (a->type->kind == P_INT || a->type->kind == P_BOOL
                || a->type->kind == P_POINTER)) {
            e->type = _new_bool();
            return 0;
        }
        break;
    case gtO:
    case ltO:
    case geqO:
    case leqO:
        if (P_type_eq(a->type, b->type) && a->type->kind == P_INT) {
            e->type = _new_bool();
            return 0;
        }
        break;
    case andO:
    case orO:
        if (P_type_eq(a->type, b->type) && a->type->kind == P_BOOL) {
            e->type = _new_bool();
            return 0;
        }
        break;
    }

    // op error
    char *a_str = P_type_str(a->type);
    char *b_str = P_type_str(b->type);
    char *op_str = _op_str(e->val.opE.op);

    fprintf(stderr, "ERR: <%i> Cannot %s types '%s' and '%s'\n", e->lineno,
            op_str, a_str, b_str);

    Free(b_str);
    Free(a_str);
    Free(op_str);

    return -1;
}

static int _check_exp(TreeVisitor *v, void *n) {
    struct _check_data *data = v->data;
    EXP *node = n;

    switch (node->kind) {
    case intconstK:
        node->type = _new_int();
        break;
    case boolconstK:
        node->type = _new_bool();
        break;
    case nullK: {
        node->type = _new_null_pointer();
        break;
    }
    case containerK: {
        EXP *inner = node->val.containerE.e;

        switch (node->val.containerE.container) {
        case negationC:
            if (inner->type->kind != P_BOOL) {
                fprintf(stderr, "ERR: <%i> Cannot ! non-bool type\n",
                        node->lineno);
                return -1;
            }
            node->type = _new_bool();
            break;
        case absC:
            if (inner->type->kind == P_INT) {
                node->type = _new_int();
                break;
            }
            if (inner->type->kind == P_POINTER) {
                P_type *to = inner->type->as_pointer.to;
                if (to->kind == P_ARRAY) {
                    node->type = _new_int();
                    break;
                }
            }
            char *inner_str = P_type_str(inner->type);
            fprintf(stderr,
                    "ERR: <%i> Cannot take abs value or size of type '%s'\n",
                    node->lineno, inner_str);
            Free(inner_str);
            return -1;
        }
        break;
    }
    case opK:
        return _check_op(node);
    case varK:
        node->type = node->val.varE->type;
        break;
    case callK: {
        Symbol *s = getSymbol(data->symbols, node->val.callE.identifier);

        assert(s != NULL); // TODO: will this EVER happen?

        if (s->kind != P_FUNC) {
            fprintf(stderr, "ERR: <%i> Cannot call non-function symbol '%s'\n",
                    node->lineno, s->name);
        }

        EXP *params_call = node->val.callE.head;
        P_NameTypePair *params_expect = s->params;
        while (params_call != NULL && params_expect != NULL) {
            if (!P_type_eq(params_call->type, params_expect->type)) {
                char *call_str = P_type_str(params_call->type);
                char *expect_str = P_type_str(params_expect->type);
                fprintf(stderr,
                        "ERR: <%i> Invalid type for parameter '%s' of "
                        "function '%s'. Expected '%s', got '%s'\n",
                        node->lineno, params_expect->name, s->name, call_str,
                        expect_str);
                Free(call_str);
                Free(expect_str);
                return -1;
            }
            params_call = params_call->next;
            params_expect = params_expect->next;
        }

        if (params_call != NULL || params_expect != NULL) {
            fprintf(
                stderr,
                "ERR: <%i> Invalid number of parameters for function '%s'\n",
                node->lineno, s->name);
            return -1;
        }

        node->type = s->type;
        break;
    }
    default:
        assert(false);
    }

    return 0;
}

static int _check_var(TreeVisitor *v, void *n) {
    struct _check_data *data = v->data;
    VAR *node = n;

    switch (node->kind) {
    case identK: {
        char *name = node->val.identV;
        Symbol *s = getSymbol(data->symbols, name);
        if (s == NULL) {
            fprintf(stderr, "ERR: <lineno unknown> Symbol %s does not exist\n",
                    name);
            return -1;
        }

        node->type = s->type;
        break;
    }
    case indexK:
    case memberK: {
        VAR *var;
        if (node->kind == indexK) {
            var = node->val.indexV.var;
        } else if (node->kind == memberK) {
            var = node->val.memberV.var;
        } else {
            assert(false); // sanity check
        }

        if (var->type->kind != P_POINTER) {
            char *type_str = P_type_str(var->type);
            fprintf(stderr,
                    "ERR: <line unknown> Cannot deref (needed "
                    "indexing array/accessing record) non-pointer "
                    "type '%s'\n",
                    type_str);
            Free(type_str);
            assert(false);
            return -1;
        }

        P_type *inner = var->type->as_pointer.to;
        assert(inner->kind == P_ARRAY
               || inner->kind == P_RECORD); // sanity check

        if (inner->kind == P_ARRAY) {
            EXP *index = node->val.indexV.index;
            if (index->type->kind != P_INT) {
                char *type_str = P_type_str(var->type);
                char *index_str = P_type_str(index->type);
                fprintf(stderr,
                        "ERR: <line unknown> Cannot take non-int "
                        "index of type '%s'. Expected int, got type "
                        "'%s'\n",
                        type_str, index_str);
                Free(type_str);
                Free(index_str);
                return -1;
            }

            node->type = inner->as_array.of_type;
        } else if (inner->kind == P_RECORD) {
            char *name = node->val.memberV.member;
            bool success = false;
            for (P_NameTypePair *it = inner->as_record.members; it != NULL;
                 it = it->next) {
                if (!strcmp(it->name, name)) {
                    node->type = it->type;
                    success = true;
                    break;
                }
            }

            if (!success) {
                char *type_str = P_type_str(var->type);
                fprintf(stderr,
                        "ERR: <line unknown> Member '%s' of "
                        "type '%s' does not exist\n",
                        name, type_str);
                Free(type_str);
                return -1;
            }
        }

        break;
    }
    default:
        assert(false);
    }

    return 0;
}

static int _typecheck(FUNC *main) {
    TreeVisitor check_visitor = {
        .data = NULL,
        .func = {.enter = _check_func_enter, .exit = _check_func_exit},
        .stmt = {.enter = _check_stmt_enter, .exit = _check_stmt_exit},
        .var = {.exit = _check_var},
        .exp = {.exit = _check_exp},
    };

    return accept_TreeNode(&check_visitor, TV_FUNC, main);
}

int typecheck(FUNC *main) {
    bool collect_result = collect_symbols(main);
    dump_types(main);
    if (collect_result) {
        // TODO: more descriptive?
        fprintf(stderr, "One or more errors in type collection\n");
        return collect_result;
    }

    bool resolve_result = resolve_types(main);
    dump_types(main);
    if (resolve_result) {
        // TODO: more descriptive?
        fprintf(stderr, "One or more errors in type resolution\n");
        return -1;
    }

    if (ensure_resolved(main)) {
        // TODO: more descriptive?
        fprintf(stderr, "Not all types have been fully resolved\n");
        return -1;
    }

    bool typecheck_result = _typecheck(main);
    if (typecheck_result) {
        // TODO: more descriptive?
        fprintf(stderr, "One or more errors in type checking\n");
        return -1;
    }

    return 0;
}
