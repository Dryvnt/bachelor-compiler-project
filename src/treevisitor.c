#include "treevisitor.h"
#include "tree.h"
#include "visitorcommon.h"

#include <assert.h>

int accept_TreeNode(TreeVisitor *visitor, TreeNodeKind kind, void *node_p) {
    int ret = 0;
    switch (kind) {
    case TV_FUNC: {
        FUNC *node = node_p;

        CALL_IF_NOT_NULL(ret, visitor->func.enter, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recurse */
        ret = accept_TreeNode(visitor, TV_VARDECL, node->head->parameters);
        RETURN_IF_ERROR(ret);

        ret = accept_TreeNode(visitor, TV_DECL, node->body->declarations);
        RETURN_IF_ERROR(ret);

        ret = accept_TreeNode(visitor, TV_STMT, node->body->statements);
        RETURN_IF_ERROR(ret);
        /* recurse over */

        CALL_IF_NOT_NULL(ret, visitor->func.exit, visitor, node);
        RETURN_IF_ERROR(ret);

        return 0;
    }

    case TV_DECL: {
        DECL *node = node_p;

        /* base case of list recursion */
        if (node == NULL) {
            return 0;
        }

        CALL_IF_NOT_NULL(ret, visitor->decl.enter, visitor, node);
        RETURN_IF_ERROR(ret);

        /* TODO: only recurse to functions? implement as user? */
        switch (node->kind) {
        case typedeclK: {
            ret = accept_TreeNode(visitor, TV_TYPEDECL, node->val.typedeclD);
            RETURN_IF_ERROR(ret);
            break;
        }
        case funcdeclK: {
            ret = accept_TreeNode(visitor, TV_FUNC, node->val.funcdeclD);
            RETURN_IF_ERROR(ret);
            break;
        }
        case vardeclK: {
            ret = accept_TreeNode(visitor, TV_VARDECL, node->val.vardeclD);
            RETURN_IF_ERROR(ret);
            break;
        }
        }

        CALL_IF_NOT_NULL(ret, visitor->decl.exit, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recurse to next decl */
        return accept_TreeNode(visitor, TV_DECL, node->next);

        return 0;
    }

    case TV_TYPEDECL: {
        TYPEDECL *node = node_p;

        CALL_IF_NOT_NULL(ret, visitor->typedecl, visitor, node);
        RETURN_IF_ERROR(ret);

        return 0;
    }

    case TV_VARDECL: {
        VARDECL *node = node_p;

        if (node == NULL) {
            return 0;
        }

        CALL_IF_NOT_NULL(ret, visitor->vardecl, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recurse to next vardecl */
        return accept_TreeNode(visitor, TV_VARDECL, node->next);
    }

    case TV_STMT: {
        STMT *node = node_p;

        CALL_IF_NOT_NULL(ret, visitor->stmt.enter, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recurse */
        switch (node->kind) {
        case returnK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.returnS);
            RETURN_IF_ERROR(ret);
            break;
        }
        case writeK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.writeS);
            RETURN_IF_ERROR(ret);
            break;
        }
        case allocK: {
            ret = accept_TreeNode(visitor, TV_VAR, node->val.allocS);
            RETURN_IF_ERROR(ret);
            break;
        }
        case allocArrayK: {
            ret = accept_TreeNode(visitor, TV_VAR, node->val.alloclengthS.var);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_EXP,
                                  node->val.alloclengthS.length);
            RETURN_IF_ERROR(ret);
            break;
        }
        case assignK: {
            ret = accept_TreeNode(visitor, TV_VAR, node->val.assignS.var);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_EXP, node->val.assignS.value);
            RETURN_IF_ERROR(ret);
            break;
        }
        case ifThenK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.ifThenS.cond);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_STMT, node->val.ifThenS.action);
            RETURN_IF_ERROR(ret);
            break;
        }
        case ifThenElseK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.ifThenElseS.cond);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_STMT,
                                  node->val.ifThenElseS.action);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_STMT,
                                  node->val.ifThenElseS.otherwise);
            RETURN_IF_ERROR(ret);
            break;
        }
        case whileK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.whileS.cond);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_STMT, node->val.whileS.action);
            RETURN_IF_ERROR(ret);
            break;
        }
        }

        CALL_IF_NOT_NULL(ret, visitor->stmt.exit, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recurse to next stmt */
        if (node->next != NULL) {
            return accept_TreeNode(visitor, TV_STMT, node->next);
        }

        return 0;
    }
    case TV_EXP: {
        EXP *node = node_p;

        if (node == NULL) {
            return 0;
        }

        CALL_IF_NOT_NULL(ret, visitor->exp.enter, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recursion */
        switch (node->kind) {
        case opK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.opE.a);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_EXP, node->val.opE.b);
            RETURN_IF_ERROR(ret);
            break;
        }
        case varK: {
            ret = accept_TreeNode(visitor, TV_VAR, node->val.varE);
            RETURN_IF_ERROR(ret);
            break;
        }
        case callK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.callE.head);
            RETURN_IF_ERROR(ret);
            break;
        }
        case containerK: {
            ret = accept_TreeNode(visitor, TV_EXP, node->val.containerE.e);
            RETURN_IF_ERROR(ret);
            break;
        }
        case intconstK:
        case boolconstK:
        case nullK: {
            break;
        }
        }

        CALL_IF_NOT_NULL(ret, visitor->exp.exit, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recurse through list (i.e. args in function call */
        ret = accept_TreeNode(visitor, TV_EXP, node->next);
        RETURN_IF_ERROR(ret);

        return 0;
    }
    case TV_VAR: {
        VAR *node = node_p;

        CALL_IF_NOT_NULL(ret, visitor->var.enter, visitor, node);
        RETURN_IF_ERROR(ret);

        /* recursion */
        switch (node->kind) {
        case identK: {
            // nothing to recurse
            break;
        }
        case indexK: {
            ret = accept_TreeNode(visitor, TV_VAR, node->val.indexV.var);
            RETURN_IF_ERROR(ret);
            ret = accept_TreeNode(visitor, TV_EXP, node->val.indexV.index);
            RETURN_IF_ERROR(ret);
            break;
        }
        case memberK: {
            ret = accept_TreeNode(visitor, TV_VAR, node->val.memberV.var);
            RETURN_IF_ERROR(ret);
            break;
        }
        }

        CALL_IF_NOT_NULL(ret, visitor->var.exit, visitor, node);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    }
    // unreachable
    assert(false);
}
