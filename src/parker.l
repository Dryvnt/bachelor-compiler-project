%{
#include <stdio.h>
#include <stdbool.h>
#include "y.tab.h"

extern int lineno;
extern int comment_nesting_level;

// To avoid warnings. It's weird
int fileno(FILE *);
%}

%option nounput noinput


%x comment
%%

#.*                         /* one-line comments */

    /* http://stackoverflow.com/questions/34493467/how-to-handle-nested-comment-in-flex */

"(*"                        { comment_nesting_level++; 
                              BEGIN(comment); }
<comment>"(*"               comment_nesting_level++;
<comment>"*"+")"            { comment_nesting_level--;
                              if(comment_nesting_level == 0) {
                                  BEGIN(INITIAL);
                              }
                            }
<comment>.                  // very inefficient
<comment>\n                 lineno++;


[ \t]+                      /* ignore whitespace */
\r                          /* ignore carriage return. windows is bs */
\n                          lineno++;

","                         return ',';
"\."                        return '.';
"="                         return '=';
"("                         return '(';
")"                         return ')';
"["                         return '[';
"]"                         return ']';
"{"                         return '{';
"}"                         return '}';
"!"                         return '!';
":"                         return ':';
";"                         return ';';


"+"                         return tPLUS;
"-"                         return tMINUS;
"*"                         return tMULT;
"/"                         return tDIV;
"=="                        return tEQ;
"!="                        return tNEQ;
">"                         return tGT;
"<"                         return tLT;
">="                        return tGEQ;
"<="                        return tLEQ;
"&&"                        return tAND;


"|"                         return '|';

"int"                       return tINTTYPE;
"bool"                      return tBOOLTYPE;
"array"                     return tARRAY; /* split into 'array' and 'of'
                                              instead of 'arrayof' to
                                              allow any whitespace instead
                                              of just space */
"record"                    return tRECORD; /* see above */
"length"                    return tLENGTH;
"of"                        return tOF;
"type"                      return tTYPE;

0|([1-9][0-9]*)           { yylval.intconst = atoi(yytext);
                            return tINTCONST; }
"false"                   { yylval.boolconst = false;
                            return tBOOLCONST; }
"true"                    { yylval.boolconst = true;
                            return tBOOLCONST; }
"null"                      return tNULL;

"write"                     return tWRITE;
"return"                    return tRETURN;
"allocate"                  return tALLOCATE;
"if"                        return tIF;
"then"                      return tTHEN;
"else"                      return tELSE;
"while"                     return tWHILE;
"do"                        return tDO;
"func"                      return tFUNC;
"end"                       return tEND;
"var"                       return tVAR;

[a-zA-Z_][a-zA-Z0-9_]*    { yylval.stringconst = (char *)malloc(strlen(yytext)+1);
                            strcpy(yylval.stringconst, yytext);
                            return tIDENTIFIER; }

.                         { fprintf(stderr, "EEEERR <%i>: Invalid token '%s' (%i)\n", lineno, yytext, yytext[0]);
                            exit(EXIT_FAILURE); }

%%
