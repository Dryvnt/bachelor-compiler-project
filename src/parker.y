%{
#include "tree.h"
#include <stdio.h>
#include <stdbool.h>

extern char *yytext;
extern int lineno;
extern FUNC *_main;

void yyerror(const char *m) {
    fprintf(stderr, "Error at line %i: %s\n", lineno, m);
}
int yylex();
%}

%error-verbose

%union {
    int intconst;
    char *stringconst;
    bool boolconst; //is an int, but clarity
    struct HEAD *head;
    struct BODY *body;
    struct TAIL *tail;
    struct TYPE *type;
    struct FUNC *function;
    struct VARDECL *var_decl;
    struct DECL *declaration;
    struct STMT *statement;
    struct EXP *exp;
    struct VAR *variable;
}

%token tRETURN tWRITE tALLOCATE tIF tTHEN tELSE tWHILE tDO tTYPE

%token tFUNC tEND tVAR

%token tINTTYPE tBOOLTYPE tARRAY tRECORD tOF tLENGTH

%token <intconst> tINTCONST
%token <boolconst> tBOOLCONST
%token <stringconst> tIDENTIFIER
%token tNULL

%type <statement> statement statement_list
%type <exp> exp term arg_list exp_list
%type <variable> variable
%type <declaration> declaration decl_list
%type <head> head
%type <body> body mainbody
%type <tail> tail
%type <type> type
%type <function> function
%type <var_decl> var_decl var_decl_list par_decl_list

%start program

%left '|'
%precedence tOR
%left tAND
%left tEQ tNEQ tGT tLT tGEQ tLEQ
%left tPLUS tMINUS
%left tMULT tDIV

/* https://www.gnu.org/software/bison/manual/html_node/Non-Operators.html#Non-Operators */
%precedence tTHEN
%precedence tELSE

%%
/* if we use regular body, ($1)->statements is const */
program         : mainbody
                { HEAD *mainhead = makeHEAD("$main", NULL, makeTYPEinttype());
                  TAIL *maintail = makeTAIL("$main");
                  _main = makeFUNC(mainhead, $1, maintail);}
;

/* have to append implicit return statement before constructing BODY node */
mainbody        : decl_list statement_list
                { // append implicit 'return 0;'
                  EXP *zero = makeEXPintconst(0);
                  STMT *ret = makeSTMTreturn(zero);
                  STMT *all = appendSTMTlist($2, ret);
                  $$ = makeBODY($1, all); }
;

function        : head body tail
                { $$ = makeFUNC($1, $2, $3); }
;

head            : tFUNC tIDENTIFIER '(' par_decl_list ')' ':' type
                { $$ = makeHEAD($2, $4, $7); }
;

tail            : tEND tIDENTIFIER
                { $$ = makeTAIL($2); }
;

type            : tIDENTIFIER
                { $$ = makeTYPEtypename($1); }
                | tINTTYPE
                { $$ = makeTYPEinttype(); }
                | tBOOLTYPE
                { $$ = makeTYPEbooltype(); }
                | tARRAY tOF type
                { $$ = makeTYPEarraytype($3); }
                | tRECORD tOF '{' var_decl_list '}'
                { $$ = makeTYPErecordtype($4); }
;

par_decl_list   : %empty
                { $$ = NULL; }
                | var_decl_list
                { $$ = $1; }

;

var_decl_list   : var_decl
                { $$ = $1; }
                | var_decl_list ',' var_decl
                { $$ = appendVARDECLlist($1, $3); }
;

var_decl        : tIDENTIFIER ':' type
                { $$ = makeVARDECL($1, $3); }
;

body            : decl_list statement_list
                { $$ = makeBODY($1, $2); }
;

decl_list       : %empty
                { $$ = NULL; }
                | decl_list declaration
                { $$ = appendDECLlist($1, $2); }
;

declaration     : tTYPE tIDENTIFIER '=' type ';'
                { $$ = makeDECLtypedecl($2, $4); }
                | function
                { $$ = makeDECLfuncdecl($1); }
                | tVAR var_decl_list ';'
                { $$ = makeDECLvardecl($2); }
;

statement_list  : statement
                { $$ = $1; }
                | statement_list statement
                { $$ = appendSTMTlist($1, $2); }
;

statement       : tRETURN exp ';'
                { $$ = makeSTMTreturn($2); }
                | tWRITE exp ';'
                { $$ = makeSTMTwrite($2); }
                | tALLOCATE variable ';'
                { $$ = makeSTMTallocate($2); }
                | tALLOCATE variable tOF tLENGTH exp ';'
                { $$ = makeSTMTallocatelength($2, $5); }
                | variable '=' exp ';'
                { $$ = makeSTMTassignment($1, $3); }
                | tIF exp tTHEN statement
                { $$ = makeSTMTifthen($2, $4); }
                | tIF exp tTHEN statement tELSE statement
                { $$ = makeSTMTifthenelse($2, $4, $6); }
                | tWHILE exp tDO statement
                { $$ = makeSTMTwhiledo($2, $4); }
                | '{' statement_list '}'
                { $$ = $2; }
;

variable        : tIDENTIFIER
                { $$ = makeVARident($1); }
                | variable '[' exp ']'
                { $$ = makeVARindex($1, $3); }
                | variable '.' tIDENTIFIER
                { $$ = makeVARmember($1, $3); }
;

/* tried to split op into own for modularity. did not work */
exp             : exp tPLUS exp
                { $$ = makeEXPop(plusO, $1, $3); }
                | exp tMINUS exp
                { $$ = makeEXPop(minusO, $1, $3); }
                | exp tMULT exp
                { $$ = makeEXPop(multO, $1, $3); }
                | exp tDIV exp
                { $$ = makeEXPop(divO, $1, $3); }
                | exp tEQ exp
                { $$ = makeEXPop(eqO, $1, $3); }
                | exp tNEQ exp
                { $$ = makeEXPop(neqO, $1, $3); }
                | exp tGT exp
                { $$ = makeEXPop(gtO, $1, $3); }
                | exp tLT exp
                { $$ = makeEXPop(ltO, $1, $3); }
                | exp tGEQ exp
                { $$ = makeEXPop(geqO, $1, $3); }
                | exp tLEQ exp
                { $$ = makeEXPop(leqO, $1, $3); }
                | exp tAND exp
                { $$ = makeEXPop(andO, $1, $3); }
                | exp '|' '|' exp %prec tOR
                { $$ = makeEXPop(orO, $1, $4); }
                | term
                { $$ = $1; }
;

term            : variable
                { $$ = makeEXPvar($1); }
                | tIDENTIFIER '(' arg_list ')' /* func call */
                { $$ = makeEXPcall($1, $3); }
                | '(' exp ')'
                { $$ = $2; }
                | '|' exp '|'
                { $$ = makeEXPcontainer(absC, $2); }
                | '!' term
                { $$ = makeEXPcontainer(negationC, $2); }
                | tINTCONST
                { $$ = makeEXPintconst($1); }
                | tBOOLCONST
                { $$ = makeEXPboolconst($1); }
                | tNULL
                { $$ = makeEXPnull(); }
;

arg_list        : %empty
                { $$ = NULL; }
                | exp_list
                { $$ = $1; }

;

exp_list        : exp
                { $$ = $1; }
                | exp_list ',' exp
                { $$ = appendEXPlist($1, $3); }
;
%%
