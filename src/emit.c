#include "emit.h"
#include "macros.h"
#include "memory.h"
#include "pir.h"

#include <assert.h>
#include <stdio.h>

static char *runtime_print_int_name = "runtime.print_int";
static char *runtime_print_bool_name = "runtime.print_bool";
static char *runtime_alloc_name = "runtime.alloc";

void emit_var(FILE *stream, PIR_var *v) {
    switch (v->kind) {
    case IMMEDIATE:
        fprintf(stream, "$%i", v->as_immediate);
        break;
    case REGISTER:
        switch (v->as_register) {
        case R_RAX:
            fprintf(stream, "%%rax");
            break;
        case R_RCX:
            fprintf(stream, "%%rcx");
            break;
        case R_RDX:
            fprintf(stream, "%%rdx");
            break;
        case R_RSI:
            fprintf(stream, "%%rsi");
            break;
        case R_RDI:
            fprintf(stream, "%%rdi");
            break;
        case R_RBP:
            fprintf(stream, "%%rbp");
            break;
        case R_RSP:
            fprintf(stream, "%%rsp");
            break;
        case R_R8:
            fprintf(stream, "%%r8");
            break;
        case R_R9:
            fprintf(stream, "%%r9");
            break;
        case R_R10:
            fprintf(stream, "%%r10");
            break;
        case R_R11:
            fprintf(stream, "%%r11");
            break;
        case R_R12:
            fprintf(stream, "%%r12");
            break;
        case R_R13:
            fprintf(stream, "%%r13");
            break;
        case R_R14:
            fprintf(stream, "%%r14");
            break;
        case R_R15:
            fprintf(stream, "%%r15");
            break;
        }
        break;
    case DISPLACEMENT:
        fprintf(stream, "%i(", v->as_displacement.i);
        emit_var(stream, v->as_displacement.var);
        fprintf(stream, ")");
        break;
    case INDEX:
        fprintf(stream, "(");
        emit_var(stream, v->as_index.of);
        fprintf(stream, ",");
        emit_var(stream, v->as_index.index);
        // ugly hex hack. we know scale <= 8 so this works
        fprintf(stream, ",0x%i)", v->as_index.scale);
        break;
    case STATIC:
        fprintf(stream, "%i(%%rbp)", v->as_static.n * -8);
        break;
    case STATIC_REF:
        fprintf(stream, "%i(", v->as_static_ref.static_var->as_static.n * -8);
        emit_var(stream, v->as_static_ref.rbp);
        fprintf(stream, ")");
        break;
    case TEMPORARY:
        // This should hard error in a real-world compiler
        // Keep this for debugging
        fprintf(stream, "<TMP%i>", v->as_temporary);
        break;
    }
}

void emit_single(FILE *stream, char *instruction, PIR_asm *s) {
    fprintf(stream, "%s ", instruction);
    emit_var(stream, s->as_single);
}

void emit_op(FILE *stream, char *instruction, PIR_asm *s) {
    fprintf(stream, "%s ", instruction);
    emit_var(stream, s->as_double.a);
    fprintf(stream, " , ");
    emit_var(stream, s->as_double.b);
}

void emit_label(FILE *stream, PIR_asm *s) {
    if (s->label == NULL) {
        fprintf(stream, "<label not yet generated>");
    } else {
        fprintf(stream, "%s", s->label);
    }
}

void emit_asm(FILE *stream, PIR_asm *s) {
    for (PIR_asm *it = s; it != NULL; it = it->next) {
        if (it->label != NULL) {
            fprintf(stream, "%s:\n", it->label);
        }
        bool print_newline = true;
        switch (it->kind) {
        case UNRESOLVED_CALL:
            // TODO: hard error
            fprintf(stream, "\t<call %s>", it->as_unresolved_call);
            break;
        case PRE_CALL:
            fprintf(stream, "\t#pre call");
            break;
        case POST_CALL:
            fprintf(stream, "\t#post call");
            break;
        case PRINT_INT_CALL:
            fprintf(stream, "\tcall %s", runtime_print_int_name);
            break;
        case PRINT_BOOL_CALL:
            fprintf(stream, "\tcall %s", runtime_print_bool_name);
            break;
        case ALLOC_CALL:
            fprintf(stream, "\tcall %s", runtime_alloc_name);
            break;
        case CALL:
            fprintf(stream, "\tcall ");
            if (it->as_call->entry_code != NULL) {
                emit_label(stream, it->as_call->entry_code);
            } else {
                fprintf(stream, "<function label not yet created>");
            }
            break;
        case NOP:
            // fprintf(stream, "\tnop");
            print_newline = false;
            break;
        case UNRESOLVED_RETURN:
            // TODO: hard error
            fprintf(stream, "\t<jump to return>");
            break;
        case RET:
            fprintf(stream, "\tret");
            break;
        case JUMP:
            fprintf(stream, "\tjmp ");
            emit_label(stream, it->as_flow);
            break;
        case JUMP_EQ:
            fprintf(stream, "\tje ");
            emit_label(stream, it->as_flow);
            break;
        case JUMP_NEQ:
            fprintf(stream, "\tjne ");
            emit_label(stream, it->as_flow);
            break;
        case JUMP_GT:
            fprintf(stream, "\tjg ");
            emit_label(stream, it->as_flow);
            break;
        case JUMP_LT:
            fprintf(stream, "\tjl ");
            emit_label(stream, it->as_flow);
            break;
        case JUMP_GEQ:
            fprintf(stream, "\tjge ");
            emit_label(stream, it->as_flow);
            break;
        case JUMP_LEQ:
            fprintf(stream, "\tjle ");
            emit_label(stream, it->as_flow);
            break;
        case JUMP_NZ:
            fprintf(stream, "\tjnz ");
            emit_label(stream, it->as_flow);
            break;
        case PUSH:
            emit_single(stream, "\tpushq", it);
            break;
        case POP:
            emit_single(stream, "\tpopq", it);
            break;
        case LOAD:
            emit_op(stream, "\tmovq", it);
            break;
        case ADD:
            emit_op(stream, "\taddq", it);
            break;
        case SUB:
            emit_op(stream, "\tsubq", it);
            break;
        case XOR:
            emit_op(stream, "\txorq", it);
            break;
        case CMP:
            emit_op(stream, "\tcmp", it);
            break;
        case IMUL:
            emit_op(stream, "\timulq", it);
            break;
        case AND:
            emit_op(stream, "\tandq", it);
            break;
        case OR:
            emit_op(stream, "\torq", it);
            break;
        case IDIV:
            fprintf(stream, "\tcqto\n"); // sign extend rax -> rdx:rax
            emit_single(stream, "\tidivq", it);
            break;
        case NEG:
            emit_single(stream, "negq", it);
            break;
        }
        if (it->comment != NULL) {
            fprintf(stream, "\t# %s", it->comment);
            print_newline = true;
        }
        if (print_newline) {
            fprintf(stream, "\n");
        }
    }
}

void emit_function(FILE *stream, PIR_function *f) {
    for (PIR_func_list *it = f->sub_functions; it != NULL; it = it->next) {
        PIR_function *sub = it->elem;
        emit_function(stream, sub);
    }

    if (f->parent == NULL) {
        fprintf(stream, ".globl main\n");
        fprintf(stream, "main:\n");
    }

    emit_asm(stream, f->entry_code);
    fprintf(stream, "\n");
    emit_asm(stream, f->inner_code);
    emit_asm(stream, f->exit_code);
    fprintf(stream, "\n");
}

static char *printf_label_name = ".Lprintf_format";
static char *printf_label_true_name = ".Lprintf_format_true";
static char *printf_label_false_name = ".Lprintf_format_false";
static char *alloc_pointer_name =
    ".Lalloc_pointer"; // our pointer into malloc block
static char *alloc_count_name =
    ".Lalloc_count"; // amount of bytes left in current block
static const int malloc_block_size =
    1 << 16; // number of bytes to allocate, 64KiB

static void emit_runtime(FILE *stream) {
    fprintf(stream, "/* BEGIN RUNTIME */\n\n");

    fprintf(stream, "/* num quadwords in %%rdi, returns pointer in rax*/\n");
    fprintf(stream, "%s:\n", runtime_alloc_name);
    fprintf(stream, "\tpushq %%rbp\n");
    fprintf(stream, "\tmovq %%rsp, %%rbp\n");
    fprintf(stream, "\tpushq %%r12\n");
    fprintf(stream, "\tpushq %%r13\n");
    fprintf(stream, "\tpushq %%r14\n");
    fprintf(stream, "\tpushq %%rdi\n");
    fprintf(stream,
            "\t/* use non-volatile registers so malloc won't mangle */\n");
    fprintf(stream, "\tmovq $%s, %%r12\n", alloc_pointer_name);
    fprintf(stream, "\tmovq $%s, %%r13\n", alloc_count_name);
    fprintf(stream, "\tmovq %%rdi, %%r14\n");
    fprintf(stream, "\timulq $8, %%r14\n");
    fprintf(stream, "\t/* if want > blocksize then exit error */\n");
    fprintf(stream, "\tcmp $%i, %%r14\n", malloc_block_size);
    fprintf(stream, "\tjle .L%s.is_valid_req\n", runtime_alloc_name);
    fprintf(stream, "\tmovq $60, %%rax # sys_exit\n");
    fprintf(stream, "\tmovq $1, %%rdi # exit code 1\n");
    fprintf(stream, "\tsyscall # exit(1)\n");
    fprintf(stream, ".L%s.is_valid_req:\n", runtime_alloc_name);
    fprintf(stream, "\n");
    fprintf(stream, "\t/* want <= avail -> don't malloc new block */\n");
    fprintf(stream, "\tcmp (%%r13), %%r14\n");
    fprintf(stream, "\tjle .L%s.after_malloc\n", runtime_alloc_name);
    fprintf(stream, "\n");
    fprintf(stream, "\tmovq $%i, %%rdi\n", malloc_block_size);
    fprintf(stream, "\tcall malloc\n");
    fprintf(stream, "\tmovq %%rax, (%%r12)\n");
    fprintf(stream, "\tmovq $%i, (%%r13)\n", malloc_block_size);
    fprintf(stream, ".L%s.after_malloc:\n", runtime_alloc_name);
    fprintf(stream, "\n");
    fprintf(stream, "\t/* very simple allocation */\n");
    fprintf(stream, "\tmovq (%%r12), %%rax\n");
    fprintf(stream, "\taddq %%r14, (%%r12)\n");
    fprintf(stream, "\tsubq %%r14, (%%r13)\n");
    fprintf(stream, "\n");
    fprintf(stream, "\tpopq %%rdi\n");
    fprintf(stream, "\tpopq %%r14\n");
    fprintf(stream, "\tpopq %%r13\n");
    fprintf(stream, "\tpopq %%r12\n");
    fprintf(stream, "\tmovq %%rbp, %%rsp\n");
    fprintf(stream, "\tpopq %%rbp\n");
    fprintf(stream, "\tret\n");
    fprintf(stream, "\n");

    fprintf(stream, "/* input in %%rdi, no return value*/\n");
    fprintf(stream, "%s:\n", runtime_print_int_name);
    fprintf(stream, "\tpushq %%rbp\n");
    fprintf(stream, "\tmovq %%rsp, %%rbp\n");
    fprintf(stream, "\tpushq %%rdi\n");
    fprintf(stream, "\tpushq %%rax\n");
    fprintf(stream, "\tpushq %%rsi\n");
    fprintf(stream, "\tmovq %%rdi, %%rsi\n");
    fprintf(stream, "\tmovq $%s, %%rdi\n", printf_label_name);
    fprintf(stream, "\tmovq $0, %%rax\n");
    fprintf(stream, "\tcall printf\n");
    fprintf(stream, "\tpopq %%rsi\n");
    fprintf(stream, "\tpopq %%rax # no error checking\n");
    fprintf(stream, "\tpopq %%rdi\n");
    fprintf(stream, "\tmovq %%rbp, %%rsp\n");
    fprintf(stream, "\tpopq %%rbp\n");
    fprintf(stream, "\tret\n");
    fprintf(stream, "\n");

    fprintf(stream, "/* input in %%rdi, no return value*/\n");
    fprintf(stream, "%s:\n", runtime_print_bool_name);
    fprintf(stream, "\tpushq %%rbp\n");
    fprintf(stream, "\tmovq %%rsp, %%rbp\n");
    fprintf(stream, "\tpushq %%rdi\n");
    fprintf(stream, "\tpushq %%rax\n");
    fprintf(stream, "\tcmp $0, %%rdi\n");
    fprintf(stream, "\tjne .L%s.else\n", runtime_print_bool_name);
    fprintf(stream, "\tmovq $%s, %%rdi\n", printf_label_false_name);
    fprintf(stream, "\tmovq $0, %%rax\n");
    fprintf(stream, "\tcall printf\n");
    fprintf(stream, "\tjmp .L%s.endif\n", runtime_print_bool_name);
    fprintf(stream, ".L%s.else:\n", runtime_print_bool_name);
    fprintf(stream, "\tmovq $%s, %%rdi\n", printf_label_true_name);
    fprintf(stream, "\tmovq $0, %%rax\n");
    fprintf(stream, "\tcall printf\n");
    fprintf(stream, ".L%s.endif:\n", runtime_print_bool_name);
    fprintf(stream, "\tpopq %%rax # no error checking\n");
    fprintf(stream, "\tpopq %%rdi\n");
    fprintf(stream, "\tmovq %%rbp, %%rsp\n");
    fprintf(stream, "\tpopq %%rbp\n");
    fprintf(stream, "\tret\n");
    fprintf(stream, "\n");

    fprintf(stream, "/* END RUNTIME */\n\n");
}

void emit(FILE *stream, PIR_function *main) {
    assert(main != NULL);

    fprintf(stream, ".data\n");
    fprintf(stream, "\n");

    fprintf(stream, "%s:\n", alloc_count_name);
    fprintf(stream, "\t.quad 0\n");
    fprintf(stream, "\n");

    fprintf(stream, "%s:\n", alloc_pointer_name);
    fprintf(stream, "\t.quad 0\n");
    fprintf(stream, "\n");

    fprintf(stream, "%s:\n", printf_label_name);
    fprintf(stream, "\t.string \"%%i\\n\"\n");
    fprintf(stream, "\n");

    fprintf(stream, "%s:\n", printf_label_true_name);
    fprintf(stream, "\t.string \"true\\n\"\n");
    fprintf(stream, "\n");

    fprintf(stream, "%s:\n", printf_label_false_name);
    fprintf(stream, "\t.string \"false\\n\"\n");
    fprintf(stream, "\n");

    fprintf(stream, ".text\n");
    fprintf(stream, "\n");

    emit_runtime(stream);

    // visitor pattern might be useful here
    emit_function(stream, main);
}
