#ifndef __typecheck_h
#define __typecheck_h

#include "tree.h"
#include "type.h"

int typecheck(FUNC *);

// declared here so symbol tree can call
void TYPECHECK_LOG(const char *, ...);

#endif //__typecheck_h
