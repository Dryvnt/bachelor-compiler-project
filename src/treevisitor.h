#ifndef __TREEVISITOR_H
#define __TREEVISITOR_H

struct TreeVisitor;

typedef int (*TreeVisitorFunc)(struct TreeVisitor *, void *);

typedef struct TreeVisitorFunctions {
    TreeVisitorFunc enter;
    TreeVisitorFunc exit;
} TreeVisitorFunctions;

typedef struct TreeVisitor {
    void *data;
    TreeVisitorFunctions func;
    TreeVisitorFunctions decl;
    TreeVisitorFunctions stmt;
    TreeVisitorFunctions var;
    TreeVisitorFunctions exp;
    TreeVisitorFunc typedecl;
    TreeVisitorFunc vardecl;
} TreeVisitor;

typedef enum {
    TV_FUNC,
    TV_DECL,
    TV_TYPEDECL,
    TV_VARDECL,
    TV_STMT,
    TV_VAR,
    TV_EXP,
} TreeNodeKind;

int accept_TreeNode(TreeVisitor *, TreeNodeKind, void *);

#endif
