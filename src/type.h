#ifndef __type__h
#define __type__h

/* Type data structure for typechecking.
   Contains functions relevant to this,
   such as type equivalence, and utility
   functions for printing.
 */

#include <stdbool.h>

typedef enum {
    P_INT,
    P_BOOL,
    P_POINTER,
    P_ARRAY,
    P_RECORD,
    P_UNRESOLVED,
    P_ERROR,
} P_TypeKind;

typedef struct P_NameTypePair {
    char *name;
    struct P_type *type;
    struct P_NameTypePair *next;
} P_NameTypePair;

typedef struct P_type {
    P_TypeKind kind;
    union {
        struct {
            char *name; // for debugging/printing
            struct P_type *to;
        } as_pointer;
        struct {
            struct P_type *of_type;
        } as_array;
        struct {
            P_NameTypePair *members;
        } as_record;
        struct {
            char *name;
        } as_unresolved;
    };
} P_type;

bool P_type_eq(P_type *, P_type *);

char *P_type_str(P_type *);

void P_type_print(P_type *);

void TYPECHECK_LOG_P_TYPE(P_type *t);

#endif
