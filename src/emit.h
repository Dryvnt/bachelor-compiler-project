#ifndef __emit_h
#define __emit_h

#include "pir.h"

#include <stdio.h>

/* Input: Incomplete/complete PIR assembly
   Output: Abstract or concrete assembly printed to stream */

void emit(FILE *stream, PIR_function *main);

#endif
