#include "hash.h"
#include "memory.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

int Hash(const char *str) {
    int partial = 0;

    for (int i = 0; str[i] != '\0'; i++) {
        partial <<= 1;
        partial += str[i];
    }

    // Should the modulo be done on the caller's side?
    return partial % HashSize;
}

HashTable *initHashTable() {
    HashTable *tmp = NEW(HashTable);

    // can avoid if NEW uses calloc
    memset(tmp, 0, sizeof(HashTable));
    return tmp;
}

static void *findInList(HashMember *m, const char *identifier) {
    for (HashMember *it = m; it != NULL; it = m->next) {
        if (!strcmp(identifier, it->identifier)) {
            return it;
        }
    }

    return NULL;
}

static HashMember *initHashMember(const char *identifier,
                                  const void *element) {
    // Lot of hoops to work around const members.. Do we want this?
    char *buf = NEWARRAY(char, strlen(identifier) + 1);
    sprintf(buf, "%s", identifier);
    HashMember tmp = {.identifier = buf, .element = element, .next = NULL};

    HashMember *m = NEW(HashMember);
    *m = tmp;
    return m;
}

void hashPutElement(HashTable *t, const char *identifier,
                    const void *element) {
    int pos = Hash(identifier);

    assert(findInList(t->table[pos], identifier) == NULL);

    // Create the symbol
    HashMember *m = initHashMember(identifier, element);

    HashMember *it = t->table[pos];
    if (it == NULL) {
        t->table[pos] = m;
    } else {
        while (it->next != NULL) {
            it = it->next;
        }
        it->next = m;
    }
}

const void *hashGetElement(const HashTable *t, const char *identifier) {
    int pos = Hash(identifier);

    HashMember *m = findInList(t->table[pos], identifier);

    if (m != NULL) {
        return m->element;
    } else {
        return NULL;
    }
}
