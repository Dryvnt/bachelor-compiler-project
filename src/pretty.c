#include "pretty.h"
#include "memory.h"
#include "treevisitor.h"

#include <assert.h>
#include <stdio.h>

#define PRETTY_PRINT(...) fprintf(stderr, __VA_ARGS__)

static void print_indent(const int indent) {
    for (int i = 0; i < indent; i++) {
        PRETTY_PRINT("    ");
    }
}

static int enter_func(TreeVisitor *v, void *n) {
    FUNC *node = n;
    int *level = v->data;

    print_indent(*level);
    PRETTY_PRINT("func %s(", node->head->identifier);

    for (VARDECL *par = node->head->parameters; par != NULL; par = par->next) {
        char *typestr = P_type_str(par->type->type_val);
        PRETTY_PRINT("%s: %s", par->identifier, typestr);
        Free(typestr);
        if (par->next != NULL) {
            PRETTY_PRINT(", ");
        }
    }

    char *typestr = P_type_str(node->head->returntype->type_val);
    PRETTY_PRINT(") : %s {\n", typestr);
    Free(typestr);

    *level += 1;

    return 0;
}

static int exit_func(TreeVisitor *v, void *n) {
    (void)n;
    int *level = v->data;

    *level -= 1;

    print_indent(*level);
    PRETTY_PRINT("}\n");
    return 0;
}

static int enter_typedecl(TreeVisitor *v, void *n) {
    TYPEDECL *node = n;
    int *level = v->data;

    print_indent(*level);
    char *typestr = P_type_str(node->type->type_val);
    PRETTY_PRINT("type %s = %s;\n", node->identifier, typestr);
    Free(typestr);
    return 0;
}

static int enter_decl(TreeVisitor *v, void *n) {
    DECL *node = n;
    int *level = v->data;

    if (node->kind == vardeclK) {
        for (VARDECL *it = node->val.vardeclD; it != NULL; it = it->next) {
            print_indent(*level);
            char *typestr = P_type_str(it->type->type_val);
            PRETTY_PRINT("var %s : %s;\n", it->identifier, typestr);
            Free(typestr);
        }
    }

    return 0;
}

static int enter_stmt(TreeVisitor *v, void *n) {
    STMT *node = n;
    int *level = v->data;
    print_indent(*level);

    PRETTY_PRINT("<STMT> ");

    switch (node->kind) {
    case returnK: {
        PRETTY_PRINT("return");
        break;
    }
    case writeK: {
        PRETTY_PRINT("write");
        break;
    }
    case allocK: {
        PRETTY_PRINT("allocate");
        break;
    }
    case allocArrayK: {
        PRETTY_PRINT("allocate of length");
        break;
    }
    case assignK: {
        PRETTY_PRINT("assign");
        break;
    }
    case ifThenK: {
        PRETTY_PRINT("if then");
        break;
    }
    case ifThenElseK: {
        PRETTY_PRINT("if then else");
        break;
    }
    case whileK: {
        PRETTY_PRINT("while");
        break;
    }
    }
    PRETTY_PRINT(":\n");

    *level += 1;
    return 0;
}

static int exit_stmt(TreeVisitor *v, void *n) {
    (void)n;
    int *level = v->data;
    *level -= 1;

    return 0;
}

static void print_OP(opType op) {
    switch (op) {
    case plusO: {
        PRETTY_PRINT("addition");
        break;
    }
    case minusO: {
        PRETTY_PRINT("subtraction");
        break;
    }
    case multO: {
        PRETTY_PRINT("multiplication");
        break;
    }
    case divO: {
        PRETTY_PRINT("division");
        break;
    }
    case eqO: {
        PRETTY_PRINT("equals");
        break;
    }
    case neqO: {
        PRETTY_PRINT("not equals");
        break;
    }
    case gtO: {
        PRETTY_PRINT("greater than");
        break;
    }
    case ltO: {
        PRETTY_PRINT("less than");
        break;
    }
    case geqO: {
        PRETTY_PRINT("greater than or equal to");
        break;
    }
    case leqO: {
        PRETTY_PRINT("less than or equal to");
        break;
    }
    case andO: {
        PRETTY_PRINT("and");
        break;
    }
    case orO: {
        PRETTY_PRINT("or");
        break;
    }
    }
}

static void print_container(expContainerType c) {
    switch (c) {
    case negationC:
        PRETTY_PRINT("negation");
    case absC:
        PRETTY_PRINT("abs");
    }
}

static int enter_exp(TreeVisitor *v, void *n) {
    EXP *node = n;
    int *level = v->data;
    print_indent(*level);

    char *typestr = P_type_str(node->type);
    PRETTY_PRINT("<EXP: %s> ", typestr);
    Free(typestr);

    switch (node->kind) {
    case opK: {
        print_OP(node->val.opE.op);
        break;
    }
    case varK: {
        PRETTY_PRINT("var");
        break;
    }
    case callK: {
        PRETTY_PRINT("call %s", node->val.callE.identifier);
        break;
    }
    case containerK: {
        print_container(node->val.containerE.container);
        break;
    }
    case intconstK: {
    case boolconstK:
        PRETTY_PRINT("const");
        break;
    }
    case nullK: {
        PRETTY_PRINT("null pointer");
        break;
    }
    }

    PRETTY_PRINT(":\n");
    *level += 1;

    // no recursion, so we have to print values here
    switch (node->kind) {
    case intconstK:
        print_indent(*level);
        PRETTY_PRINT("%i\n", node->val.intconstE);
        break;
    case boolconstK:
        print_indent(*level);
        PRETTY_PRINT("%s\n", node->val.boolconstE ? "true" : "false");
        break;
    case nullK:
        // do nothing??
        break;
    default:
        // otherwise recurse
        break;
    }

    return 0;
}

static int exit_exp(TreeVisitor *v, void *n) {
    (void)n;
    int *level = v->data;

    *level -= 1;

    return 0;
}

static int enter_var(TreeVisitor *v, void *n) {
    VAR *node = n;
    int *level = v->data;

    print_indent(*level);
    *level += 1;
    char *typestr = P_type_str(node->type);
    PRETTY_PRINT("<VAR: %s> ", typestr);
    Free(typestr);

    switch (node->kind) {
    case identK: {
        PRETTY_PRINT("ident");
        break;
    }
    case indexK: {
        PRETTY_PRINT("index");
        break;
    }
    case memberK: {
        PRETTY_PRINT("member '%s'", node->val.memberV.member);
        break;
    }
    }
    PRETTY_PRINT(":\n");

    // no recursion, so we have to print value here
    if (node->kind == identK) {
        print_indent(*level);
        PRETTY_PRINT("%s\n", node->val.identV);
    }

    return 0;
}

static int exit_var(TreeVisitor *v, void *n) {
    (void)n;
    int *level = v->data;

    *level -= 1;

    return 0;
}

void prettyAST(FUNC *main) {
    int level = 0;
    TreeVisitor visitor = {
        .data = &level,
        .func = {.enter = enter_func, // handles VARDECL
                 .exit = exit_func},
        .decl = {.enter = enter_decl}, // handles VARDECL
        .typedecl = enter_typedecl,
        .stmt = {.enter = enter_stmt, .exit = exit_stmt},
        .var = {.enter = enter_var, .exit = exit_var},
        .exp = {.enter = enter_exp, .exit = exit_exp},
    };

    assert(accept_TreeNode(&visitor, TV_FUNC, main) == 0);
}
