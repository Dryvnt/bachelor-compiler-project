#include "tree.h"
#include "typecheck.h"

#include <assert.h>
#include <stdio.h>

int lineno = 0;
bool arg_treelog = false;
bool arg_typechecklog = false;

/*
    Really hard to make good tests, considering the amount of
    boilerplate for anything non-trivial
*/

void int_comparison_makes_bool() {
    EXP *ten = makeEXPintconst(10);
    EXP *nine = makeEXPintconst(9);
    EXP *op = makeEXPop(gtO, ten, nine);

    BODY *body = makeBODY(NULL, makeSTMTwrite(op));
    FUNC *f = makeFUNC(makeHEAD("test", NULL, makeTYPEinttype()), body, NULL);

    typecheck(f);

    assert(ten->type->kind == P_INT);
    assert(nine->type->kind == P_INT);
    assert(op->type->kind == P_BOOL);
}

void addition_is_int() {
    EXP *ten = makeEXPintconst(10);
    EXP *nine = makeEXPintconst(9);
    EXP *op = makeEXPop(plusO, ten, nine);

    BODY *body = makeBODY(NULL, makeSTMTwrite(op));
    FUNC *f = makeFUNC(makeHEAD("test", NULL, makeTYPEinttype()), body, NULL);

    typecheck(f);

    assert(ten->type->kind == P_INT);
    assert(nine->type->kind == P_INT);
    assert(op->type->kind == P_INT);
}

int main() {
    printf("int_comparison_makes_bool... ");
    int_comparison_makes_bool();
    printf("OK\n");

    printf("addition_is_int... ");
    addition_is_int();
    printf("OK\n");
}