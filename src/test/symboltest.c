#include "hash.h"
#include "symbol.h"
#include "type.h"

#include <assert.h>
#include <stdio.h>

/*
   so symbol.o doesn't complain
   this should probably be more seperated
   in the first place, but oh well
 */
void TYPECHECK_LOG(void *a, ...) { (void)a; }
void TYPECHECK_LOG_P_TYPE(P_type *t) { (void)t; }

/* Use lineno variable to hold int arguments to differentiate symbols */

void hashCorrectnessTest() { assert(Hash("kitty") == (3369 % HashSize)); }

// This is sorta iffy since the symbol table stores pointers,
// not int values. Some ugly casts
void scopeGivesClosestVariable() {
    SymbolTable *a = initSymbolTable();
    SymbolTable *b = scopeSymbolTable(a);
    SymbolTable *c = scopeSymbolTable(b);

    putSymbol(a, "x", P_VAR, NULL, 5);
    putSymbol(a, "y", P_VAR, NULL, 41);
    putSymbol(b, "x", P_VAR, NULL, 3);
    putSymbol(b, "n", P_VAR, NULL, 14);
    putSymbol(b, "p", P_VAR, NULL, 3);

    int a_x_level;
    Symbol *a_x = getSymbolLevel(a, "x", &a_x_level);
    assert(a_x_level == 0);
    assert(a_x->lineno == 5);

    int b_x_level;
    Symbol *b_x = getSymbolLevel(b, "x", &b_x_level);
    assert(b_x_level == 0);
    assert(b_x->lineno == 3);

    int c_x_level;
    Symbol *c_x = getSymbolLevel(c, "x", &c_x_level);
    assert(c_x_level == 1);
    assert(c_x->lineno == 3);
}

void getSymbolReturnsNull() {
    SymbolTable *a = initSymbolTable();
    SymbolTable *b = scopeSymbolTable(a);

    Symbol *x = getSymbol(b, "x");

    assert(x == NULL);
}

// Blocks are used to help understand structure of generated table
void manySubScopesPossible() {
    SymbolTable *t[10];

    t[0] = initSymbolTable();

    t[1] = scopeSymbolTable(t[0]);
    t[2] = scopeSymbolTable(t[0]);
    t[3] = scopeSymbolTable(t[0]);

    t[4] = scopeSymbolTable(t[1]);

    t[5] = scopeSymbolTable(t[2]);
    t[6] = scopeSymbolTable(t[2]);
    t[7] = scopeSymbolTable(t[2]);

    t[8] = scopeSymbolTable(t[6]);
    t[9] = scopeSymbolTable(t[6]);

    for (int i = 0; i < 10; i++) {
        putSymbol(t[i], "x", P_VAR, NULL, i);
    }
}

int main() {
    printf("hashCorrectnessTest... ");
    hashCorrectnessTest();
    printf("OK\n");

    printf("scopeGivesClosestVariable... ");
    scopeGivesClosestVariable();
    printf("OK\n");

    printf("getSymbolReturnsNull... ");
    getSymbolReturnsNull();
    printf("OK\n");

    printf("manySubScopesPossible... ");
    manySubScopesPossible();
    printf("OK\n");

    return 0;
}