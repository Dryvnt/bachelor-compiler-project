#include "memory.h"
#include "type.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

bool arg_typechecklog = false;

int accept_Type(void *a, void *b) {
    (void)a;
    (void)b;
    return 0;
}

static void basic_equivalence() {
    P_type *a = NEW(P_type);
    P_type *b = NEW(P_type);

    a->kind = P_INT;
    b->kind = P_INT;
    assert(P_type_eq(a, b));

    a->kind = P_INT;
    b->kind = P_BOOL;
    assert(!P_type_eq(a, b));

    a->kind = P_ARRAY;
    b->kind = P_RECORD;
    assert(!P_type_eq(a, b));

    Free(a);
    Free(b);
}

static void array_equivalence() {
    P_type *tmp1 = NEW(P_type);
    P_type *tmp2 = NEW(P_type);
    tmp1->kind = P_ARRAY;
    tmp2->kind = P_ARRAY;

    P_type *tmp3 = NEW(P_type);
    P_type *tmp4 = NEW(P_type);
    tmp3->kind = P_INT;
    tmp4->kind = P_BOOL;

    tmp1->as_array.of_type = tmp3;
    tmp2->as_array.of_type = tmp4;

    P_type *a = NEW(P_type);
    P_type *b = NEW(P_type);

    a->kind = P_POINTER;
    b->kind = P_POINTER;
    a->as_pointer.to = tmp1;
    b->as_pointer.to = tmp2;
    assert(!P_type_eq(a, b));
    tmp4->kind = P_INT;
    assert(P_type_eq(a, b));

    Free(a);
    Free(b);
    Free(tmp1);
    Free(tmp2);
    Free(tmp3);
    Free(tmp4);
}

static void record_equivalence() {
    P_type *tmp1 = NEW(P_type);
    P_type *tmp2 = NEW(P_type);

    tmp1->kind = P_INT;
    tmp2->kind = P_RECORD;
    tmp2->as_record.members = NEW(P_NameTypePair);
    tmp2->as_record.members->name = "i";
    tmp2->as_record.members->type = tmp2;

    P_type *tmp3 = NEW(P_type);
    P_type *tmp4 = NEW(P_type);

    tmp3->kind = P_INT;
    tmp4->kind = P_RECORD;
    tmp4->as_record.members = NEW(P_NameTypePair);
    tmp4->as_record.members->name = "i";
    tmp4->as_record.members->type = tmp3;

    P_type *a = NEW(P_type);
    P_type *b = NEW(P_type);

    a->kind = P_POINTER;
    b->kind = P_POINTER;
    a->as_pointer.to = tmp2;
    b->as_pointer.to = tmp4;

    /* No structural equivalence */
    assert(!P_type_eq(a, b));
    b->as_pointer.to = tmp2;
    /* But name equivalence */
    assert(P_type_eq(a, b));

    Free(a);
    Free(b);
    Free(tmp1);
    Free(tmp2->as_record.members);
    Free(tmp2);
    Free(tmp3);
    Free(tmp4->as_record.members);
    Free(tmp4);
}

int main() {
    printf("basic_equivalence... ");
    basic_equivalence();
    printf("OK\n");
    printf("array_equivalence... ");
    array_equivalence();
    printf("OK\n");
    printf("record_equivalence... ");
    record_equivalence();
    printf("OK\n");

    return 0;
}