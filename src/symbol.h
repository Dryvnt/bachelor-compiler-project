#ifndef __symbol_h
#define __symbol_h

#include "hash.h"
#include "tree.h"
#include "type.h"

#include <stdbool.h>

typedef enum { P_VAR, P_FUNC, P_TYPE } SymbolKind;

typedef struct Symbol {
    int lineno; // line of declaration
    const char *name;
    SymbolKind kind;
    P_type *type;
    P_NameTypePair *params;
    // Below is for code generation
    union {
        // void pointers are icky. Can we avoid somehow?
        void *as_func; // PIR_function
        void *as_var;  // PIR_var
    };
} Symbol;

typedef struct NameList {
    char *name;
    struct NameList *next;
} NameList;

typedef struct SymbolTable {
    HashTable *symbols;
    NameList *names_it;
    struct SymbolTable *parent;
} SymbolTable;

SymbolTable *initSymbolTable();
SymbolTable *scopeSymbolTable(SymbolTable *);
Symbol *putSymbol(SymbolTable *, const char *, SymbolKind, P_type *, int);
Symbol *getSymbol(SymbolTable *, const char *);
Symbol *getSymbolLevel(SymbolTable *, const char *, int *);
void dumpSymbolTable(const SymbolTable *);

void SYMBOL_LOG_SYMBOL(const Symbol *);

#endif
