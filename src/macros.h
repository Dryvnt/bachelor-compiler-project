#ifndef __macros_h
#define __macros_h

// shortening boilerplate
// Weird make-expression-on-stack-then-copy-into-heap
// pattern to uniformly use designated initializers
#define NEW_FROM_FUNCTION(type)                   \
    static type *new_##type##_from(type source) { \
        type *out = NEW(type);                    \
        *out = source;                            \
        return out;                               \
    }

// Used during implementation. More explanative than just assert(false);
#define unimplemented()                                               \
    do {                                                              \
        fprintf(stderr, "unimplemented line %i, file %s\n", __LINE__, \
                __FILE__);                                            \
        assert(false);                                                \
    } while (0)

#endif