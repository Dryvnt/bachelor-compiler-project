#ifndef __VISITORCOMMON_H
#define __VISITORCOMMON_H

#define CALL_IF_NOT_NULL(retvar, f, ...) \
    do {                                 \
        if (f != NULL) {                 \
            retvar = f(__VA_ARGS__);     \
        }                                \
    } while (0)

#define RETURN_IF_ERROR(retvar) \
    do {                        \
        if (retvar) {           \
            return retvar;      \
        }                       \
    } while (0)

#endif
