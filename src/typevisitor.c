#include "typevisitor.h"
#include "type.h"
#include "visitorcommon.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

int _accept_NameTypePair(TypeVisitor *visitor, P_NameTypePair *p) {
    if (p == NULL) {
        return 0;
    }

    int ret = 0;

    CALL_IF_NOT_NULL(ret, visitor->p_nametypepair.enter, visitor, p);
    RETURN_IF_ERROR(ret);

    /* recurse */
    ret = accept_Type(visitor, p->type);
    RETURN_IF_ERROR(ret);

    CALL_IF_NOT_NULL(ret, visitor->p_nametypepair.exit, visitor, p);
    RETURN_IF_ERROR(ret);

    // next pair
    ret = _accept_NameTypePair(visitor, p->next);
    RETURN_IF_ERROR(ret);

    return 0;
}

int accept_Type(TypeVisitor *visitor, P_type *type) {
    int ret = 0;

    switch (type->kind) {
    case P_INT: {
        CALL_IF_NOT_NULL(ret, visitor->p_int, visitor, type);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    case P_BOOL: {
        CALL_IF_NOT_NULL(ret, visitor->p_bool, visitor, type);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    case P_POINTER: {
        CALL_IF_NOT_NULL(ret, visitor->p_pointer, visitor, type);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    case P_ARRAY: {
        CALL_IF_NOT_NULL(ret, visitor->p_array.enter, visitor, type);
        RETURN_IF_ERROR(ret);

        ret = accept_Type(visitor, type->as_array.of_type);
        RETURN_IF_ERROR(ret);

        CALL_IF_NOT_NULL(ret, visitor->p_array.exit, visitor, type);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    case P_RECORD: {
        CALL_IF_NOT_NULL(ret, visitor->p_record.enter, visitor, type);
        RETURN_IF_ERROR(ret);

        /* recurse */
        _accept_NameTypePair(visitor, type->as_record.members);

        CALL_IF_NOT_NULL(ret, visitor->p_record.exit, visitor, type);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    case P_UNRESOLVED: {
        CALL_IF_NOT_NULL(ret, visitor->p_unresolved, visitor, type);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    case P_ERROR: {
        CALL_IF_NOT_NULL(ret, visitor->p_error, visitor, type);
        RETURN_IF_ERROR(ret);
        return 0;
    }
    }

    assert(false); // UNREACHABLE
}
