#include "memory.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *appendString(char *to, const char *s) {
    to = Realloc(to, strlen(to) + strlen(s) + 1);
    strcpy(to + strlen(to), s);
    return to;
}

// clone implies we alloc new string
char *cloneString(const char *s) {
    char *out = NEWARRAY(char, strlen(s) + 1);
    strcpy(out, s);

    assert(strcmp(out, s) == 0);

    return out;
}

void *Calloc(unsigned int n, unsigned int size) {
    void *p;
    if (!(p = calloc(n, size))) {
        fprintf(stderr, "Calloc(%d) failed.\n", n);
        fflush(stderr);
        abort();
    }
    return p;
}

void *Realloc(void *p, unsigned n) {
    if (!(p = realloc(p, n))) {
        fprintf(stderr, "Realloc(%d) failed.\n", n);
        fflush(stderr);
        abort();
    }
    return p;
}

void Free(void *p) { free(p); }
