#include "type.h"
#include "memory.h"
#include "typevisitor.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

extern bool arg_typechecklog;

bool P_type_eq(P_type *a, P_type *b) {
    if (a->kind != b->kind) {
        return false;
    }

    switch (a->kind) {
    case P_INT:
    case P_BOOL:
        return true; // primitive types always eq
    case P_POINTER:
        // if only one is null, infer the type
        if (a->as_pointer.to == NULL) {
            a->as_pointer.to = b->as_pointer.to;
            a->as_pointer.name = cloneString(b->as_pointer.name);
        }
        if (b->as_pointer.to == NULL) {
            b->as_pointer.to = a->as_pointer.to;
            b->as_pointer.name = cloneString(a->as_pointer.name);
        }

        // skip recursion if pointer is the same
        // also handles both being null pointers, which is fine
        if (a->as_pointer.to == b->as_pointer.to) {
            return true;
        }

        return P_type_eq(a->as_pointer.to, b->as_pointer.to);
    case P_ARRAY:
        return P_type_eq(a->as_array.of_type, b->as_array.of_type);
    case P_RECORD:
        // TODO: structural equiv
        // for now: pointer/name equivalence
        return a == b;
    case P_UNRESOLVED:
    case P_ERROR:
        // we should not be getting these if we passed typechecking
        // passing typechecking is a requirement to get here
        assert(false);
    }
    // unreachable
    assert(false);
}

static int _str_int(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_INT);
    v->data = appendString(v->data, "int");
    return 0;
}

static int _str_bool(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_BOOL);
    v->data = appendString(v->data, "bool");
    return 0;
}

static int _str_pointer(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_POINTER);
    if (t->as_pointer.to == NULL) {
        v->data = appendString(v->data, "null pointer");
    } else {
        v->data = appendString(v->data, "pointer to ");
        v->data = appendString(v->data, t->as_pointer.name);
    }
    return 0;
}

static int _str_array_enter(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_ARRAY);
    v->data = appendString(v->data, "heap-based array of [ ");
    return 0;
}

static int _str_array_exit(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_ARRAY);
    v->data = appendString(v->data, " ]");
    return 0;
}

static int _str_nametypepair_enter(TypeVisitor *v, P_NameTypePair *p) {
    v->data = appendString(v->data, p->name);
    v->data = appendString(v->data, " : ");
    return 0;
}

static int _str_nametypepair_exit(TypeVisitor *v, P_NameTypePair *p) {
    if (p->next == NULL) {
        v->data = appendString(v->data, " ");
    } else {
        v->data = appendString(v->data, ", ");
    }
    return 0;
}

static int _str_record_enter(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_RECORD);
    v->data = appendString(v->data, "heap-based record of { ");
    return 0;
}

static int _str_record_exit(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_RECORD);
    v->data = appendString(v->data, "} ");
    return 0;
}

static int _str_unresolved(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_UNRESOLVED);
    v->data = appendString(v->data, "unresolved ( ");
    v->data = appendString(v->data, t->as_unresolved.name);
    v->data = appendString(v->data, " )");
    return 0;
}

static int _str_error(TypeVisitor *v, P_type *t) {
    assert(t->kind == P_ERROR);
    v->data = appendString(v->data, "error");
    return 0;
}

char *P_type_str(P_type *t) {
    if (t == NULL) {
        // TODO: error?
        return cloneString("<TYPE IS NULL>");
    }

    TypeVisitor str_visitor = {
        .data = cloneString(""),
        .p_int = _str_int,
        .p_bool = _str_bool,
        .p_pointer = _str_pointer,
        .p_array = {.enter = _str_array_enter, .exit = _str_array_exit},
        .p_record = {.enter = _str_record_enter, .exit = _str_record_exit},
        .p_unresolved = _str_unresolved,
        .p_error = _str_error,
        .p_nametypepair = {.enter = _str_nametypepair_enter,
                           .exit = _str_nametypepair_exit},
    };

    accept_Type(&str_visitor, t);

    return str_visitor.data;
}

void P_type_print(P_type *t) {
    char *s = P_type_str(t);
    fprintf(stderr, "%s", s);
    Free(s);
}

void TYPECHECK_LOG_P_TYPE(P_type *t) {
    if (arg_typechecklog) {
        P_type_print(t);
    }
}
