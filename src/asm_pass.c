#include "codegen.h"
#include "macros.h"
#include "memory.h"
#include "pir.h"
#include "symbol.h"
#include "tree.h"
#include "treevisitor.h"

#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

extern bool arg_secondlog;

static void SECOND_PASS_LOG(const char *fmt, ...) {
    if (arg_secondlog) {
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
    }
}

static void static_ref_to_static_expand(PIR_function *f, bool *dirty,
                                        PIR_asm *prev, PIR_asm *curr,
                                        PIR_var *var, PIR_register reg) {
    if (var->kind == STATIC_REF) {
        PIR_var *rbp_var = var->as_static_ref.rbp;
        if (rbp_var->kind == STATIC) {
            code_builder insert = {0};
            push_asm(&insert, make_PIR_asm_double(LOAD, rbp_var,
                                                  make_PIR_var_register(reg)));
            var->as_static_ref.rbp = make_PIR_var_register(reg);

            if (prev == NULL) {
                f->inner_code = insert.head;
            } else {
                prev->next = insert.head;
            }
            insert.tail->next = curr;

            *dirty = true;
        }
    }
}

static void mark_variable_in_array(bool *array, PIR_var *var,
                                   bool follow_static) {
    switch (var->kind) {
    case REGISTER:
        if (var->as_register < NUM_REGISTERS_ALLOC) {
            array[var->as_register] = true;
        }

        break;
    case TEMPORARY:
        array[var->as_temporary + NUM_REGISTERS_ALLOC] = true;
        break;
    case STATIC_REF:
        if (follow_static) {
            mark_variable_in_array(array, var->as_static_ref.rbp,
                                   follow_static);
        }
        break;
    case STATIC: {
        PIR_var *rbp = make_PIR_var_register(R_RBP);
        mark_variable_in_array(array, rbp, follow_static); // does nothing
        Free(rbp);
    }
    case IMMEDIATE:
        break;
    case INDEX:
    case DISPLACEMENT:
        assert(false); // do we even use these?
        break;
    }
}

static void mark_uses(PIR_asm *s) {
    switch (s->kind) {
    case LOAD:
        mark_variable_in_array(s->register_alloc_data.uses, s->as_double.a,
                               true);
        if (s->as_double.b->kind == STATIC
            || s->as_double.b->kind == STATIC_REF) {
            mark_variable_in_array(s->register_alloc_data.uses, s->as_double.b,
                                   true);
        }
        break;
    case ADD:
    case SUB:
    case XOR:
    case CMP:
    case IMUL:
    case AND:
    case OR:
        mark_variable_in_array(s->register_alloc_data.uses, s->as_double.a,
                               true);
        mark_variable_in_array(s->register_alloc_data.uses, s->as_double.b,
                               true);
        break;
    case PUSH:
    case NEG:
        mark_variable_in_array(s->register_alloc_data.uses, s->as_single,
                               true);
        break;
    case IDIV: {
        PIR_var *rax = make_PIR_var_register(R_RAX);
        PIR_var *rdx = make_PIR_var_register(R_RDX);
        mark_variable_in_array(s->register_alloc_data.uses, s->as_single,
                               true);
        mark_variable_in_array(s->register_alloc_data.uses, rax, true);
        mark_variable_in_array(s->register_alloc_data.uses, rdx, true);
        Free(rdx);
        Free(rax);
        break;
    }
    case PRINT_INT_CALL:
    case PRINT_BOOL_CALL:
    case ALLOC_CALL: {
        PIR_var *rdi = make_PIR_var_register(R_RDI);
        mark_variable_in_array(s->register_alloc_data.uses, rdi, true);
        Free(rdi);
        break;
    }
    case RET:
    case UNRESOLVED_RETURN: {
        PIR_var *rax = make_PIR_var_register(R_RAX);
        mark_variable_in_array(s->register_alloc_data.uses, rax, true);
        Free(rax);
        break;
    }
    case POP:
    case CALL:
    case NOP:
    case JUMP:
    case JUMP_EQ:
    case JUMP_NEQ:
    case JUMP_GT:
    case JUMP_LT:
    case JUMP_GEQ:
    case JUMP_LEQ:
    case JUMP_NZ:
    case POST_CALL:
    case PRE_CALL:
        break;
    case UNRESOLVED_CALL:
        assert(false);
        break;
    }
}

static void mark_defines(PIR_asm *s) {
    switch (s->kind) {
    case LOAD:
    case ADD:
    case SUB:
    case XOR:
    case IMUL:
    case AND:
    case OR:
        mark_variable_in_array(s->register_alloc_data.defines, s->as_double.b,
                               false);
        break;
    case IDIV:
    case NEG:
    case POP:
        mark_variable_in_array(s->register_alloc_data.defines, s->as_single,
                               false);
        break;
    case CALL:
    case PRINT_INT_CALL:
    case PRINT_BOOL_CALL:
    case ALLOC_CALL: {
        /* volatile may or may not be set to something now get destroyed
         * completely */
        PIR_var *rax = make_PIR_var_register(R_RAX);
        mark_variable_in_array(s->register_alloc_data.defines, rax, false);
        Free(rax);
        break;
    }
    case CMP:
    case PUSH:
    case RET:
    case UNRESOLVED_RETURN:
    case NOP:
    case JUMP:
    case JUMP_EQ:
    case JUMP_NEQ:
    case JUMP_GT:
    case JUMP_LT:
    case JUMP_GEQ:
    case JUMP_LEQ:
    case JUMP_NZ:
    case POST_CALL:
    case PRE_CALL:
        break;
    case UNRESOLVED_CALL:
        assert(false);
        break;
    }
}

typedef struct PIR_asm_list {
    PIR_asm *elem;
    struct PIR_asm_list *next;
} PIR_asm_list;

static PIR_asm_list *asm_successor(PIR_asm *s) {
    PIR_asm_list *succ = NEW(PIR_asm_list);

    switch (s->kind) {
    case LOAD:
    case ADD:
    case SUB:
    case XOR:
    case IMUL:
    case AND:
    case OR:
    case IDIV:
    case NEG:
    case CMP:
    case PUSH:
    case POP:
    case CALL:
    case NOP:
    case PRINT_INT_CALL:
    case PRINT_BOOL_CALL:
    case ALLOC_CALL:
    case PRE_CALL:
    case POST_CALL:
        succ->elem = s->next;
        break;
    case JUMP:
        succ->elem = s->as_flow;
        break;
    case JUMP_EQ:
    case JUMP_NEQ:
    case JUMP_GT:
    case JUMP_LT:
    case JUMP_GEQ:
    case JUMP_LEQ:
    case JUMP_NZ: {
        succ->elem = s->next;
        PIR_asm_list *n = NEW(PIR_asm_list);
        n->elem = s->as_flow;
        succ->next = n;
        break;
    }

    case RET:
    case UNRESOLVED_RETURN:
        break;
    case UNRESOLVED_CALL:
        assert(false);
        break;
    }

    if (succ->elem == NULL) {
        Free(succ);
        return NULL;
    } else {
        return succ;
    }
}

static void array_assign(int size, bool *target, bool *source) {
    for (int i = 0; i < size; i++) {
        target[i] = source[i];
    }
}

static void array_union(int size, bool *target, bool *a, bool *b) {
    for (int i = 0; i < size; i++) {
        target[i] = a[i] || b[i];
    }
}

static void array_minus(int size, bool *target, bool *a, bool *b) {
    for (int i = 0; i < size; i++) {
        target[i] = a[i] && (!b[i]);
    }
}

static void allocate_resources(PIR_function *f) {
    SECOND_PASS_LOG("-----Allocating for %s\n", f->name);

    f->stack_variables = 0;
    for (PIR_var_list *it = f->temporary_variables; it != NULL;
         it = it->next) {
        it->elem->kind = TEMPORARY;
    }

    for (PIR_var_list *it = f->temporary_variables; it != NULL;
         it = it->next) {
        if (it->elem->escaping) {
            /*
            PIR_LOG("\t\t<TMP%i> is escaping, making static\n",
                    it->elem->as_temporary);
            */
            f->stack_variables += 1;

            it->elem->kind = STATIC;
            it->elem->as_static.n = f->stack_variables;
        }
    }

    // -2 to remove rbp and rsp
    int array_size = f->num_temporaries + NUM_REGISTERS_ALLOC;
    /* array[0]...array[15]: register #n.
       array[n + 16]: temporary #n
    */

    int num_instr = 0;
    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        if (it->register_alloc_data.uses != NULL) {
            Free(it->register_alloc_data.uses);
            Free(it->register_alloc_data.defines);
            Free(it->register_alloc_data.X);
            Free(it->register_alloc_data.Y);
        }

        it->register_alloc_data.array_size = array_size;
        /* we use calloc so arrays are initialized to false */
        it->register_alloc_data.uses = NEWARRAY(bool, array_size);
        it->register_alloc_data.defines = NEWARRAY(bool, array_size);
        it->register_alloc_data.X = NEWARRAY(bool, array_size);
        it->register_alloc_data.Y = NEWARRAY(bool, array_size);

        mark_uses(it);
        mark_defines(it);
        num_instr += 1;
    }

    bool dirty;
    do {
        dirty = false;

        /* single application of lattice function */
        SECOND_PASS_LOG("Applying lattice...\n");
        for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
            bool *tmp = NEWARRAY(bool, array_size);

            PIR_asm_list *succ = asm_successor(it);
            while (succ != NULL) {
                array_union(array_size, tmp, tmp,
                            succ->elem->register_alloc_data.X);
                PIR_asm_list *prev = succ;
                succ = succ->next;

                Free(prev);
            }

            array_minus(array_size, tmp, tmp, it->register_alloc_data.defines);
            array_union(array_size, tmp, tmp, it->register_alloc_data.uses);
            array_assign(array_size, it->register_alloc_data.Y, tmp);

            Free(tmp);

            for (int i = 0; i < array_size; i++) {
                if (it->register_alloc_data.Y[i]
                    != it->register_alloc_data.X[i]) {
                    dirty = true;
                }
            }

            array_assign(array_size, it->register_alloc_data.X,
                         it->register_alloc_data.Y);
        }
    } while (dirty);

    int count = 0;
    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        SECOND_PASS_LOG("Statement %i (Kind %i)\n", count, it->kind);
        SECOND_PASS_LOG("\tUses: { ");
        for (int i = 0; i < array_size; i++) {
            bool val = it->register_alloc_data.uses[i];
            if (val) {
                SECOND_PASS_LOG("%i, ", i);
            }
        }
        SECOND_PASS_LOG("}\n");
        SECOND_PASS_LOG("\tDefines: { ");
        for (int i = 0; i < array_size; i++) {
            bool val = it->register_alloc_data.defines[i];
            if (val) {
                SECOND_PASS_LOG("%i, ", i);
            }
        }
        SECOND_PASS_LOG("}\n");
        SECOND_PASS_LOG("\tX: { ");
        for (int i = 0; i < array_size; i++) {
            bool val = it->register_alloc_data.X[i];
            if (val) {
                SECOND_PASS_LOG("%i, ", i);
            }
        }
        SECOND_PASS_LOG("}\n");
        count += 1;
    }

    /* edges[i][j] = true if node i,j connected */
    bool edges[array_size][array_size];
    for (int i = 0; i < array_size; i++) {
        for (int j = 0; j < array_size; j++) {
            edges[i][j] = false;
        }
    }

    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        for (int i = 0; i < array_size; i++) {
            for (int j = 0; j < array_size; j++) {
                bool a = it->register_alloc_data.X[i];
                bool b = it->register_alloc_data.X[j];
                if (a && b) {
                    edges[i][j] = true;
                }
            }
        }
    }

    SECOND_PASS_LOG("Edges:\n");
    for (int i = 0; i < array_size; i++) {
        for (int j = 0; j < array_size; j++) {
            SECOND_PASS_LOG("%i ", edges[i][j]);
        }
        SECOND_PASS_LOG("\n");
    }

    int stack[array_size];
    int stack_size = 0;
    for (int i = 0; i < array_size; i++) {
        stack[i] = -1;
    }

    int vertices[array_size];
    int num_vertices = array_size;
    for (int i = 0; i < array_size; i++) {
        vertices[i] = i;
    }

    if (f->coloring != NULL) {
        Free(f->coloring);
    }
    f->coloring = NEWARRAY(int, array_size);
    int *coloring = f->coloring;
    for (int i = 0; i < array_size; i++) {
        if (i < NUM_REGISTERS_ALLOC) {
            coloring[i] = i;
        } else {
            coloring[i] = -1;
        }
    }

    int K = NUM_REGISTERS_ALLOC;
    while (num_vertices > 0) {
        bool did_simplification = false;
        for (int i = 0; i < num_vertices; i++) {
            int vertex = vertices[i];
            int num_edges = 0;
            for (int j = 0; j < num_vertices; j++) {
                int other_vertex = vertices[j];
                if (edges[vertex][other_vertex]) {
                    num_edges += 1;
                }
            }
            if (num_edges <= K - 1) {
                int tmp = vertices[num_vertices - 1];
                vertices[num_vertices - 1] = vertex;
                vertices[i] = tmp;
                num_vertices -= 1;

                stack[stack_size] = vertex;
                stack_size += 1;

                did_simplification = true;
            }
        }
        if (!did_simplification) {
            int last = vertices[num_vertices - 1];
            coloring[last] = -2; // mark for spill
            num_vertices -= 1;
        }
    }

    SECOND_PASS_LOG("stack: ");
    for (int i = 0; i < stack_size; i++) {
        SECOND_PASS_LOG("%i ", stack[i]);
    }
    SECOND_PASS_LOG("\n");
    SECOND_PASS_LOG("leftover vertices: ");
    for (int i = 0; i < num_vertices; i++) {
        SECOND_PASS_LOG("%i ", vertices[i]);
    }
    SECOND_PASS_LOG("\n");

    for (int i = stack_size - 1; i >= 0; i--) {
        int vertex = stack[i];
        int color = coloring[vertex];
        if (color == -1) {
            for (int try_color = 0; try_color < K; try_color++) {
                bool viable = true;
                for (int j = 0; j < array_size; j++) {
                    if (edges[vertex][j]) {
                        if (coloring[j] == try_color) {
                            viable = false;
                            break;
                        }
                    }
                }
                if (viable) {
                    coloring[vertex] = try_color;
                    break;
                }
            }
        }
    }

    SECOND_PASS_LOG("coloring: ");
    for (int i = 0; i < array_size; i++) {
        SECOND_PASS_LOG("%i ", coloring[i]);
    }
    SECOND_PASS_LOG("\n");

    for (PIR_var_list *it = f->temporary_variables; it != NULL;
         it = it->next) {
        PIR_var *v = it->elem;
        if (v->kind == TEMPORARY) {
            int vertex = v->as_temporary + NUM_REGISTERS_ALLOC;
            int color = coloring[vertex];
            assert(color != -1);
            if (color == -2) {
                f->stack_variables += 1;

                it->elem->kind = STATIC;
                it->elem->as_static.n = f->stack_variables;
            } else {
                v->kind = REGISTER;
                v->as_register = color;
            }
        }
    }

    /* Use R15, R14, R13 to fix things like "movq (%rax) (%rcx)" */
    do {
        dirty = false;

        PIR_asm *prev = NULL;
        for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
            switch (it->kind) {
            case LOAD:
            case ADD:
            case SUB:
            case CMP:
            case IMUL:
            case XOR:
            case AND:
            case OR: {
                PIR_var *a = it->as_double.a;
                PIR_var *b = it->as_double.b;
                static_ref_to_static_expand(f, &dirty, prev, it, a, R_R15);
                if (dirty) {
                    break;
                }
                static_ref_to_static_expand(f, &dirty, prev, it, b, R_R15);
                if (dirty) {
                    break;
                }

                if (a->kind == STATIC || a->kind == STATIC_REF) {
                    if (b->kind == STATIC || b->kind == STATIC_REF) {
                        PIR_asm *above = make_PIR_asm_double(
                            LOAD, a, make_PIR_var_register(R_R14));

                        it->as_double.a = make_PIR_var_register(R_R14);

                        above->next = it;
                        if (prev == NULL) {
                            f->inner_code = above;
                        } else {
                            prev->next = above;
                        }

                        dirty = true;
                    }
                }
                if (dirty) {
                    break;
                }

                break;
            }
            default:
                break;
            }
            switch (it->kind) {
            case IDIV:
            case NEG: {
                PIR_var *arg = it->as_single;
                if (arg->kind == STATIC || arg->kind == STATIC_REF) {
                    PIR_asm *above = make_PIR_asm_double(
                        LOAD, arg, make_PIR_var_register(R_R13));
                    PIR_asm *below = make_PIR_asm_double(
                        LOAD, make_PIR_var_register(R_R13), arg);

                    it->as_single = make_PIR_var_register(R_R13);

                    above->next = it;
                    below->next = it->next;
                    it->next = below;
                    if (prev == NULL) {
                        f->inner_code = above;
                    } else {
                        prev->next = above;
                    }
                }

                break;
            }
            case IMUL: {
                PIR_var *b = it->as_double.b;
                if (b->kind == STATIC || b->kind == STATIC_REF) {
                    PIR_asm *above = make_PIR_asm_double(
                        LOAD, b, make_PIR_var_register(R_R13));
                    PIR_asm *below = make_PIR_asm_double(
                        LOAD, make_PIR_var_register(R_R13), b);

                    it->as_double.b = make_PIR_var_register(R_R13);

                    above->next = it;
                    below->next = it->next;
                    it->next = below;
                    if (prev == NULL) {
                        f->inner_code = above;
                    } else {
                        prev->next = above;
                    }
                }

                break;
            }
            default:
                break;
            }
            prev = it;
        }
    } while (dirty);

    for (PIR_func_list *it = f->sub_functions; it != NULL; it = it->next) {
        allocate_resources(it->elem);
    }
}

/* Function entrance code generation */
static PIR_asm *codegen_func_entry(PIR_function *f) {
    code_builder entry_code = {0};

    // Assume all parameters passed by stack
    /* Stack coming in (growing down):
        - caller's stuff (do not touch)
        - last parameter
        - ...
        - first parameter
        - %rbp of above scope (static link)
        - return address <- %rsp
    */

    /* Do:
           pushq %rbp
           movq %rsp, %rbp
    */
    push_asm(&entry_code,
             make_PIR_asm_single(PUSH, make_PIR_var_register(R_RBP)));
    push_asm(&entry_code,
             make_PIR_asm_double(LOAD, make_PIR_var_register(R_RSP),
                                 make_PIR_var_register(R_RBP)));
    /* Stack now:
        - last parameter <- ((n + 2) * 8)(%rbp)
        - ...
        - second parameter <- 32(%rbp)
        - first parameter <- 24(%rbp)
        - %rbp of above scope <- 16(%rbp)
        - return address
        - caller's %rbp <- %rbp/%rsp
    */

    /* Do: (n = number of vars/temps we use in frame)
        subq $(n * 8), %rsp */
    if (f->stack_variables > 0) {
        push_asm(&entry_code,
                 make_PIR_asm_double(
                     SUB, make_PIR_var_immediate(f->stack_variables * 8),
                     make_PIR_var_register(R_RSP)));
        PIR_asm_comment(entry_code.tail,
                        "space for stack-based vars and temps");
    }

    /* Push callee-save registers we use */
    // no callee-saving implemented
    // this is where it would go.
    // Mark which registers are used in function,
    // add them here if used

    /* Stack now:
        - ...
        - %rbp of above scope
        - return address
        - caller's %rbp <- %rbp
        - first temp stackspace -8(%rbp)
        - second temp stackspace -16(%rbp)
        - ...
        - last temp stackspace -(n * 8)(%rbp)
        - callee-save register 1
        - ...
        - callee-save register k <- %rsp
    */

    return entry_code.head;
}

PIR_asm *codegen_func_exit(PIR_function *f) {
    (void)f;
    code_builder exit_code = {0};

    /* Stack now:
        - ...
        - %rbp of above scope
        - return address
        - caller's %rbp <- %rbp
        - first temp stackspace -8(%rbp)
        - second temp stackspace -16(%rbp)
        - ...
        - last temp stackspace -(n * 8)(%rbp)
        - callee-save register 1 -((n + 1) * 8)(%rbp)
        - ...
        - callee-save register k -((n + k) * 8)(%rbp)
        (assume stack is dirty)
        - garbage
        - ...
        - more garbage <- %rsp
    */

    // no callee-saving implemented
    // this is where it would go.
    // Mark which registers are used in function,
    // add them here if used
    /* Restore callee-save registers w/o pop:
        movq -((n + 1) * 8)(%rbp), <reg1>
        ...
        movq -((n + k) * 8)(%rbp), <regk>
    Alternatively clean stack then pop:
        movq %rbp, <tmp>
        subq $((n + k) * 8), <tmp>
        movq <tmp>, %rsp
        pop <regk>
        ...
        pop <reg1>

    */

    /*
        movq %rbp, %rsp
    */
    push_asm(&exit_code,
             make_PIR_asm_double(LOAD, make_PIR_var_register(R_RBP),
                                 make_PIR_var_register(R_RSP)));

    /* Stack now:
        - ...
        - %rbp of above scope
        - return address
        - caller's %rbp <- %rbp/%rsp
        - garbage space we don't care about
        - because it is beyond %rsp
    */

    /* Do:
        popq %rbp
        ret
    */

    push_asm(&exit_code,
             make_PIR_asm_single(POP, make_PIR_var_register(R_RBP)));

    /* Stack now:
       - caller's caller's %rbp <- %rbp
       - ...
       - %rbp of above scope
       - return address <- %rsp
       - garbage space we don't care about
       - because it is beyond %rsp
    */

    /* Do:
       ret
    */

    push_asm(&exit_code, make_PIR_asm_none(RET));

    /* Stack now:
       - caller's caller's %rbp <- %rbp
       - ...
       - %rbp of above scope <- %rsp
       - garbage space we don't care about
       - because it is beyond %rsp
    */

    /* Caller then cleans up parameters and %rbp
       of above scope as they see fit. Our duty
       is done, function properly returned.
    */

    /* Done exit function */
    return exit_code.head;
}

static void create_entry_exit(PIR_function *f) {
    f->entry_code = codegen_func_entry(f);
    f->exit_code = codegen_func_exit(f);

    // this is an awkward way to resolve returns
    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        if (it->kind == UNRESOLVED_RETURN) {
            it->kind = JUMP;
            it->as_flow = f->exit_code;
        }
    }

    for (PIR_func_list *it = f->sub_functions; it != NULL; it = it->next) {
        create_entry_exit(it->elem);
    }
}

static void generate_labels(PIR_function *f) {
    // entry and exit should not have control flow instructions
    {
        assert(f->entry_code->label == NULL);
        char *tmp = cloneString(f->name);
        f->entry_code->label = tmp;
    }
    {
        assert(f->exit_code->label == NULL);
        char *tmp = cloneString(".L");
        tmp = appendString(tmp, f->name);
        tmp = appendString(tmp, "_exit");
        f->exit_code->label = tmp;
    }

    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        switch (it->kind) {
        case JUMP:
        case JUMP_EQ:
        case JUMP_NEQ:
        case JUMP_GT:
        case JUMP_LT:
        case JUMP_GEQ:
        case JUMP_LEQ:
        case JUMP_NZ: {
            char buf[32];
            sprintf(buf, "%x", (unsigned int)(size_t)it->as_flow);
            if (it->as_flow->label == NULL) {
                char *tmp = cloneString(".L");
                tmp = appendString(tmp, buf);
                it->as_flow->label = tmp;
            }
            break;
        }
        default:
            break;
        }
    }

    for (PIR_func_list *it = f->sub_functions; it != NULL; it = it->next) {
        generate_labels(it->elem);
    }
}

void save_call_variables(PIR_function *f) {
    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        if (it->kind == PRE_CALL) {
            bool save[NUM_REGISTERS_ALLOC];
            for (int i = 0; i < NUM_REGISTERS_ALLOC; i++) {
                save[i] = false;
            }

            PIR_asm *call = it->as_pre_call.call;

            bool save_any = false;
            for (int i = 0; i < f->num_temporaries + NUM_REGISTERS_ALLOC;
                 i++) {
                // if temporary/register X is live before call
                if (call->register_alloc_data.X[i]) {
                    int color = f->coloring[i];
                    // don't save spilled variables and don't spill RAX
                    if (color != -2 && color != R_RAX) {
                        save[color] = true;
                        save_any = true;
                    }
                }
            }

            if (save_any) {
                code_builder pre_code = {0};
                code_builder post_code = {0};
                for (int i = 0; i < NUM_REGISTERS_ALLOC; i++) {
                    if (save[i]) {
                        push_asm(&pre_code,
                                 make_PIR_asm_single(
                                     PUSH, make_PIR_var_register(i)));
                    }
                }
                for (int i = NUM_REGISTERS_ALLOC - 1; i >= 0; i--) {
                    if (save[i]) {
                        push_asm(&post_code,
                                 make_PIR_asm_single(
                                     POP, make_PIR_var_register(i)));
                    }
                }

                PIR_asm *post = it->as_pre_call.post;
                // manual insert code
                pre_code.tail->next = it->next;
                it->next = pre_code.head;
                post_code.tail->next = post->next;
                post->next = post_code.head;
            }
        }
    }
    for (PIR_func_list *it = f->sub_functions; it != NULL; it = it->next) {
        save_call_variables(it->elem);
    }
}

static void remove_unnecessary_load(PIR_function *f) {
    for (PIR_asm *it = f->inner_code; it != NULL; it = it->next) {
        if (it->kind == LOAD) {
            PIR_var *a = it->as_double.a;
            PIR_var *b = it->as_double.b;
            if (a->kind == REGISTER && b->kind == REGISTER) {
                if (a->as_register == b->as_register) {
                    it->kind = NOP;
                }
            }
        }
    }

    for (PIR_func_list *it = f->sub_functions; it != NULL; it = it->next) {
        remove_unnecessary_load(it->elem);
    }
}

void asm_pass(PIR_function *main) {
    PIR_LOG("Allocating resources...\n");
    allocate_resources(main);
    PIR_LOG("Saving variables from calls...\n");
    save_call_variables(main);
    PIR_LOG("Generating entry and exit code...\n");
    create_entry_exit(main);
    PIR_LOG("Generating labels...\n");
    generate_labels(main);
    remove_unnecessary_load(main);
}
