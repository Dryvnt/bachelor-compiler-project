#ifndef __hash_h
#define __hash_h

/* Generic hashing table. Only used for symbol checking, but
   extracted just in case a hash table would be useful another time.
   It was not. */

#define HashSize 317

typedef struct HashMember {
    const char *identifier;
    const void *element;
    struct HashMember *next;
} HashMember;

// Generalized hashtable
typedef struct HashTable { HashMember *table[HashSize]; } HashTable;

HashTable *initHashTable();
void hashPutElement(HashTable *, const char *, const void *);
const void *hashGetElement(const HashTable *, const char *);

int Hash(const char *);

#endif
