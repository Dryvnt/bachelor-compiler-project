#include "pir.h"
#include "macros.h"
#include "memory.h"

#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>

// TODO: extern
extern bool arg_pirlog;

void PIR_LOG(const char *fmt, ...) {
    if (arg_pirlog) {
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
    }
}

void push_asm(code_builder *b, PIR_asm *code) {
    if (code == NULL) {
        return;
    }

    if (b->head == NULL) {
        b->head = code;
    }

    if (b->tail != NULL) {
        b->tail->next = code;
    }

    b->tail = code;

    // code is list of instructions. Move tail to last instr
    while (b->tail->next != NULL) {
        b->tail = b->tail->next;
    }
}

NEW_FROM_FUNCTION(PIR_var)
NEW_FROM_FUNCTION(PIR_asm)

#define ALLOW_CASE_SET_STR(case_val, str) \
    case case_val:                        \
        str = #case_val;                  \
        break

const char *reg_str(PIR_register reg) {
    const char *regstr;
    switch (reg) {
        ALLOW_CASE_SET_STR(R_RAX, regstr);
        ALLOW_CASE_SET_STR(R_RCX, regstr);
        ALLOW_CASE_SET_STR(R_RDX, regstr);
        ALLOW_CASE_SET_STR(R_RSI, regstr);
        ALLOW_CASE_SET_STR(R_RDI, regstr);
        ALLOW_CASE_SET_STR(R_RBP, regstr);
        ALLOW_CASE_SET_STR(R_RSP, regstr);
        ALLOW_CASE_SET_STR(R_R8, regstr);
        ALLOW_CASE_SET_STR(R_R9, regstr);
        ALLOW_CASE_SET_STR(R_R10, regstr);
        ALLOW_CASE_SET_STR(R_R11, regstr);
        ALLOW_CASE_SET_STR(R_R12, regstr);
        ALLOW_CASE_SET_STR(R_R13, regstr);
        ALLOW_CASE_SET_STR(R_R14, regstr);
        ALLOW_CASE_SET_STR(R_R15, regstr);
    }
    return regstr;
}

PIR_var *make_PIR_var_immediate(int i) {
    PIR_var tmp = {
        .kind = IMMEDIATE, .as_immediate = i,
    };

    PIR_var *e = new_PIR_var_from(tmp);
    PIR_LOG("%p: IMMEDIATE(%i)\n", (void *)e, i);
    return e;
}

PIR_var *make_PIR_var_register(PIR_register reg) {
    PIR_var tmp = {
        .kind = REGISTER, .as_register = reg,
    };

    const char *regstr = reg_str(reg);

    PIR_var *e = new_PIR_var_from(tmp);
    PIR_LOG("%p: REGISTER(%s)\n", (void *)e, regstr);
    return e;
}

PIR_var *make_PIR_var_static(int n) {
    PIR_var tmp = {
        .kind = STATIC,
        .as_static =
            {
                .n = n,
            },
    };

    PIR_var *e = new_PIR_var_from(tmp);
    PIR_LOG("%p: STATIC(%i)\n", (void *)e, n);
    return e;
}

PIR_var *make_PIR_var_static_ref(PIR_var *rbp, PIR_var *static_var) {
    assert(static_var->kind != REGISTER);
    static_var->escaping = true;

    PIR_var tmp = {
        .kind = STATIC_REF,
        .as_static_ref =
            {
                .rbp = rbp, .static_var = static_var,
            },
    };

    PIR_var *e = new_PIR_var_from(tmp);
    PIR_LOG("%p: STATIC_REF(%p, %p)\n", (void *)e, (void *)rbp,
            (void *)static_var);
    return e;
}

PIR_var *make_PIR_var_static_ref_const(PIR_var *rbp, int i) {
    PIR_var *tmp = make_PIR_var_static(i);
    return make_PIR_var_static_ref(rbp, tmp);
}

PIR_var *make_PIR_var_displacement(int i, PIR_var *var) {
    PIR_var tmp = {
        .kind = DISPLACEMENT, .as_displacement = {.i = i, .var = var},
    };

    PIR_var *e = new_PIR_var_from(tmp);
    PIR_LOG("%p: DISPLACEMENT(%i, %p)\n", (void *)e, i, (void *)var);
    return e;
}

/* Return value:
    Positive:
        Store in register. Value given by
        PIR_register reg = return_value;
    Negative:
        Store in stack. static offset from rbp given by
        int offset = return_value;
*/
int get_parameter_placement(int arg_num) {
    // TODO: parameters in registers?
    return -(arg_num + 2);
}

PIR_var *make_PIR_var_index(PIR_var *of, PIR_var *index, int scale) {
    switch (scale) {
    case 1:
    case 2:
    case 4:
    case 8:
        break;
    default:
        // scale not allowed
        assert(false);
    }

    PIR_var tmp = {
        .kind = INDEX,
        .as_index =
            {
                .of = of, .index = index, .scale = scale,
            },
    };

    PIR_var *e = new_PIR_var_from(tmp);
    PIR_LOG("%p: INDEX(%p, %p, %i)\n", (void *)e, (void *)of, (void *)index,
            scale);
    return e;
}

PIR_var *make_PIR_var_temporary(PIR_function *context) {
    assert(context != NULL);

    PIR_var tmp = {
        .kind = TEMPORARY, .as_temporary = context->num_temporaries,
    };

    PIR_var *e = new_PIR_var_from(tmp);

    PIR_var_list *l = NEW(PIR_var_list);
    l->elem = e;
    l->next = context->temporary_variables;
    context->temporary_variables = l;

    PIR_LOG("%p: TEMPORARY(%p) (%i)\n", (void *)e, context->num_temporaries);
    context->num_temporaries += 1;
    return e;
}

void PIR_asm_comment(PIR_asm *a, char *comment) {
    assert(a->comment == NULL);

    a->comment = cloneString(comment);
}

PIR_asm *make_PIR_asm_print_int_call() {
    PIR_asm tmp = {
        .kind = PRINT_INT_CALL,
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: PRINT_INT_CALL(%s)\n", (void *)s);
    return s;
}

PIR_asm *make_PIR_asm_print_bool_call() {
    PIR_asm tmp = {
        .kind = PRINT_BOOL_CALL,
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: PRINT_BOOL_CALL(%s)\n", (void *)s);
    return s;
}

PIR_asm *make_PIR_asm_pre_call(PIR_asm *call, PIR_asm *post) {
    PIR_asm tmp = {
        .kind = PRE_CALL, .as_pre_call = {.call = call, .post = post},
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: PRE_CALL(%p, %p)\n", (void *)s, (void *)call, (void *)post);
    return s;
}

PIR_asm *make_PIR_asm_unresolved_call(char *identifier) {
    PIR_asm tmp = {
        .kind = UNRESOLVED_CALL, .as_unresolved_call = cloneString(identifier),
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: UNRESOLVED_CALL(%s)\n", (void *)s, identifier);
    return s;
}

PIR_asm *make_PIR_asm_none(PIR_asm_kind kind) {
    char *kindstr;
    switch (kind) {
        ALLOW_CASE_SET_STR(NOP, kindstr);
        ALLOW_CASE_SET_STR(PRINT_INT_CALL, kindstr);
        ALLOW_CASE_SET_STR(PRINT_BOOL_CALL, kindstr);
        ALLOW_CASE_SET_STR(ALLOC_CALL, kindstr);
        ALLOW_CASE_SET_STR(UNRESOLVED_RETURN, kindstr);
        ALLOW_CASE_SET_STR(POST_CALL, kindstr);
        ALLOW_CASE_SET_STR(RET, kindstr);
    default:
        assert(false); // kind is not valid single
    }

    PIR_asm tmp = {
        .kind = kind,
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: %s()\n", (void *)s, kindstr);
    return s;
}

PIR_asm *make_PIR_asm_flow(PIR_asm_kind kind, PIR_asm *dest) {
    char *kindstr;
    switch (kind) {
        ALLOW_CASE_SET_STR(JUMP, kindstr);
        ALLOW_CASE_SET_STR(JUMP_EQ, kindstr);
        ALLOW_CASE_SET_STR(JUMP_NEQ, kindstr);
        ALLOW_CASE_SET_STR(JUMP_GT, kindstr);
        ALLOW_CASE_SET_STR(JUMP_LT, kindstr);
        ALLOW_CASE_SET_STR(JUMP_GEQ, kindstr);
        ALLOW_CASE_SET_STR(JUMP_LEQ, kindstr);
        ALLOW_CASE_SET_STR(JUMP_NZ, kindstr);
    default:
        assert(false); // kind is not valid flow
    }

    PIR_asm tmp = {
        .kind = kind, .as_flow = dest,
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: %s(%p)\n", (void *)s, kindstr, (void *)dest);
    return s;
}

PIR_asm *make_PIR_asm_single(PIR_asm_kind kind, PIR_var *v) {
    char *kindstr;
    switch (kind) {
        ALLOW_CASE_SET_STR(PUSH, kindstr);
        ALLOW_CASE_SET_STR(POP, kindstr);
        ALLOW_CASE_SET_STR(IDIV, kindstr);
        ALLOW_CASE_SET_STR(NEG, kindstr);
    default:
        assert(false); // kind is not valid single
    }

    PIR_asm tmp = {
        .kind = kind, .as_single = v,
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: %s(%p)\n", (void *)s, kindstr, (void *)v);
    return s;
}

PIR_asm *make_PIR_asm_double(PIR_asm_kind kind, PIR_var *a, PIR_var *b) {
    char *kindstr;
    switch (kind) {
        ALLOW_CASE_SET_STR(LOAD, kindstr);
        ALLOW_CASE_SET_STR(ADD, kindstr);
        ALLOW_CASE_SET_STR(SUB, kindstr);
        ALLOW_CASE_SET_STR(XOR, kindstr);
        ALLOW_CASE_SET_STR(CMP, kindstr);
        ALLOW_CASE_SET_STR(IMUL, kindstr);
        ALLOW_CASE_SET_STR(AND, kindstr);
        ALLOW_CASE_SET_STR(OR, kindstr);
    default:
        assert(false); // kind is not valid single
    }

    PIR_asm tmp = {
        .kind = kind, .as_double = {.a = a, .b = b},
    };

    PIR_asm *s = new_PIR_asm_from(tmp);
    PIR_LOG("%p: %s(%p, %p)\n", (void *)s, kindstr, (void *)a, (void *)b);
    return s;
}
