#ifndef __codegen_h
#define __codegen_h

#include "pir.h"
#include "tree.h"

/* Input: Abstract syntax tree
   Output: Abstract x86 assembly

   Ideally the input would be some higher-level
   IR, because a lot of typechecking-type checks
   are bound to be repeated, and there is no
   opportunity to do many pre-ASM optimizations
*/
PIR_function *codegen(FUNC *);

#endif
