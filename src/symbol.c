#include "symbol.h"
#include "memory.h"
#include "typecheck.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

void SYMBOL_LOG_SYMBOL(const Symbol *s) {
    TYPECHECK_LOG("'%s' ", s->name);
    switch (s->kind) {
    case P_VAR:
        TYPECHECK_LOG("(P_VAR): ");
        break;
    case P_FUNC:
        TYPECHECK_LOG("(P_FUNC): ");
        break;
    case P_TYPE:
        TYPECHECK_LOG("(P_TYPE): ");
        break;
    }

    TYPECHECK_LOG_P_TYPE(s->type);
}

SymbolTable *initSymbolTable() {
    SymbolTable *table = NEW(SymbolTable);

    table->symbols = initHashTable();
    table->names_it = NULL;
    table->parent = NULL;

    return table;
}

SymbolTable *scopeSymbolTable(SymbolTable *t) {
    assert(t != NULL);
    SymbolTable *table = initSymbolTable();

    // The one mutation of parent we are allowed
    table->parent = t;

    return table;
}

static Symbol *initSymbol(const char *name, SymbolKind kind, P_type *type,
                          int lineno) {
    char *buf = cloneString(name);

    Symbol tmp = {.name = buf, .kind = kind, .type = type, .lineno = lineno};
    Symbol *sym = NEW(Symbol);
    *sym = tmp;

    return sym;
}

Symbol *putSymbol(SymbolTable *t, const char *name, SymbolKind kind,
                  P_type *type, int lineno) {
    // TODO: is this error?
    assert(hashGetElement(t->symbols, name) == NULL);

    Symbol *sym = initSymbol(name, kind, type, lineno);
    hashPutElement(t->symbols, name, (void *)sym);

    NameList tmp = {.name = cloneString(name), .next = t->names_it};
    NameList *entry = NEW(NameList);
    *entry = tmp;

    t->names_it = entry;

    return sym;
}

Symbol *getSymbolLevel(SymbolTable *t, const char *name, int *level) {
    int i = 0;
    for (const SymbolTable *it = t; it != NULL; it = it->parent) {
        Symbol *sym = (Symbol *)hashGetElement(it->symbols, name);
        if (sym != NULL) {
            *level = i;
            return sym;
        }
        i++;
    }

    return NULL;
}

Symbol *getSymbol(SymbolTable *t, const char *name) {
    int level; // discard
    return getSymbolLevel(t, name, &level);
}

// Seems like O(n log n)-ish :/
void dumpSymbolTable(const SymbolTable *t) {
    // Base case for recursion
    if (t == NULL) {
        return;
    }

    // Print table above
    dumpSymbolTable(t->parent);

    // Find indentation level (tables up until root)
    int indentation = 0;
    for (const SymbolTable *it = t->parent; it != NULL; it = it->parent) {
        indentation++;
    }

    // Print
    for (int i = 0; i < HashSize; i++) {
        HashMember *m = t->symbols->table[i]; // icky
        if (m != NULL) {
            Symbol *sym = (Symbol *)m->element; // ew
            for (int j = 0; j < indentation; j++) {
                printf("  ");
            }
            printf("%s: %p\n", sym->name, (void *)sym);
        }
    }
}
