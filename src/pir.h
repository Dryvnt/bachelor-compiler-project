#ifndef __pir_h
#define __pir_h

/* Contains data structure and function definitions relevant to
   assembly generation. Named PIR (Parker IR) because it was
   intended to be a proper IR at first. */

void PIR_LOG(const char *fmt, ...);

#include "symbol.h"

#include <stdbool.h>

struct PIR_asm;

typedef struct code_builder {
    struct PIR_asm *head;
    struct PIR_asm *tail;
} code_builder;

void push_asm(code_builder *b, struct PIR_asm *code);

struct PIR_function;

// Don't use RBP, RSP. Dedicate R15, R14, R13 to resolving too-many-references
// errors
#define NUM_REGISTERS_ALLOC 10

typedef enum {
    R_RAX = 0,
    R_RCX = 1,
    R_RDX = 2,
    R_RSI = 3,
    R_RDI = 4,
    R_R8 = 5,
    R_R9 = 6,
    R_R10 = 7,
    R_R11 = 8,
    R_R12 = 9,
    R_R13 = 10,
    R_R14 = 11,
    R_R15 = 12,
    R_RBP = 13,
    R_RSP = 14,
} PIR_register;

/* Does not allocate */
const char *reg_str(PIR_register);

/* Catch-all for all types of variables in abstract
   and concrete asm.
*/
typedef struct PIR_var {
    enum {
        IMMEDIATE, // intconst, boolconst
        REGISTER,
        STATIC,     // we don't want vars to be displacements, this is more
                    // explaining
        STATIC_REF, // Variant of displacement. Displacement amount determined
                    // during emit?
        INDEX,      // e.g. (%r8, %r9, 0x8) for r8[r9]
        DISPLACEMENT, // e.g. -8(%r9)
        TEMPORARY,    // Resolve during second pass
    } kind;
    bool escaping; // true implies must be on stack (DISPLACEMENT)
    int as_temporary;
    union {
        int as_immediate;
        PIR_register as_register;
        struct {
            int i;
            struct PIR_var *var; // must be register
        } as_displacement;
        struct {
            int n; // which number static variable this is
        } as_static;
        struct {
            struct PIR_var *static_var;
            struct PIR_var *rbp;
        } as_static_ref;
        struct {
            struct PIR_var *of;
            struct PIR_var *index;
            int scale;
        } as_index;
    };
} PIR_var;

/* Return value of get_parameter_placement
    Positive:
        Store in register. Value given by
        PIR_register reg = return_value;
    Negative:
        Store in stack. Value given by
        int rbp_displacement = -return_value;
*/
int get_parameter_placement(int arg_num);

PIR_var *make_PIR_var_immediate(int i);
PIR_var *make_PIR_var_register(PIR_register reg);
PIR_var *make_PIR_var_static(int n);
PIR_var *make_PIR_var_static_ref(PIR_var *rbp, PIR_var *static_var);
PIR_var *make_PIR_var_static_ref_const(PIR_var *rbp, int i);
PIR_var *make_PIR_var_displacement(int i, PIR_var *var);
PIR_var *make_PIR_var_index(PIR_var *of, PIR_var *index, int scale);
PIR_var *make_PIR_var_temporary(struct PIR_function *context);

/* Terminology:
    special: below does not apply
    none: Has no arguments
    flow: Some sort of flow-control instruction.
    single: Has one argument
    double: Has two arguments
*/

typedef enum {
    /* special */
    UNRESOLVED_CALL = 0,
    CALL = 1,
    PRE_CALL = 2, // mark/data for saving registers before-call
    /* none */
    NOP = 3,
    PRINT_INT_CALL = 4,
    PRINT_BOOL_CALL = 5,
    ALLOC_CALL = 6,
    UNRESOLVED_RETURN = 7, // jump to end of function. changed during second
                           // pass
    POST_CALL = 8,         // marker
    RET = 9,
    /* flow */
    JUMP = 10,
    JUMP_EQ = 11,
    JUMP_NEQ = 12,
    JUMP_GT = 13,
    JUMP_LT = 14,
    JUMP_GEQ = 15,
    JUMP_LEQ = 16,
    JUMP_NZ = 17, // not zero
    /* single */
    PUSH = 18,
    POP = 19,
    IDIV = 20,
    NEG = 21,
    /* double */
    LOAD = 22,
    ADD = 23,
    SUB = 24,
    XOR = 25,
    CMP = 26,
    IMUL = 27,
    AND = 28,
    OR = 29,
} PIR_asm_kind;

typedef struct PIR_var_list {
    PIR_var *elem;
    struct PIR_var_list *next;
} PIR_var_list;

typedef struct PIR_asm {
    char *comment; // NULL implies no comment
    char *label;   // NULL implies no label (generated during second pass)
    struct PIR_function *func;
    PIR_asm_kind kind;
    union {
        char *as_unresolved_call;
        struct PIR_function *as_call;
        struct {
            struct PIR_asm *post;
            struct PIR_asm *call;
        } as_pre_call;
        struct PIR_asm *as_flow;
        PIR_var *as_single;
        struct {
            PIR_var *a;
            PIR_var *b;
        } as_double;
    };
    struct {
        int array_size;
        bool *uses;
        bool *defines;
        bool *X;
        bool *Y;
    } register_alloc_data;
    struct PIR_asm *next;
} PIR_asm;

void PIR_asm_comment(PIR_asm *a, char *comment);

PIR_asm *make_PIR_asm_unresolved_call(char *);
PIR_asm *make_PIR_asm_pre_call(PIR_asm *call, PIR_asm *post);
PIR_asm *make_PIR_asm_none(PIR_asm_kind);
PIR_asm *make_PIR_asm_flow(PIR_asm_kind, PIR_asm *dest);
PIR_asm *make_PIR_asm_single(PIR_asm_kind, PIR_var *v);
PIR_asm *make_PIR_asm_double(PIR_asm_kind, PIR_var *a, PIR_var *b);

typedef struct PIR_func_list {
    struct PIR_function *elem;
    struct PIR_func_list *next;
} PIR_func_list;

typedef struct PIR_param_list {
    char *elem;
    struct PIR_param_list *next;
} PIR_param_list;

// An abstract collection of asm
typedef struct PIR_function {
    struct PIR_function *parent;
    char *name; // for label generation
    SymbolTable *symbols;
    PIR_func_list *sub_functions;
    PIR_param_list *params;

    int stack_variables;
    int num_temporaries;
    PIR_var_list *temporary_variables; // assign during second pass
    int *coloring; // register assignment for temporary variable coloring[n]

    PIR_var_list
        *static_link_variables; // holds foreign RBPs found during
                                // static link resolution; don't repeat work

    PIR_asm *inner_code; // created during first pass
    PIR_asm *entry_code; // created during second pass
    PIR_asm *exit_code;  // created during second pass
} PIR_function;

#endif
