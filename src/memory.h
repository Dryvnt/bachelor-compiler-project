#ifndef __memory_h
#define __memory_h

/* Common memory and string operations */

// reallocates
char *appendString(char *, const char *);
char *cloneString(const char *);

void *Calloc(unsigned int n, unsigned int size);
void *Realloc(void *, unsigned int);
void Free(void *);

#define NEW(type) NEWARRAY(type, 1)
#define NEWARRAY(type, size) (type *)Calloc(size, sizeof(type))

#endif