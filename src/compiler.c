#include "asm_pass.h"
#include "codegen.h"
#include "emit.h"
#include "pir.h"
#include "pretty.h"
#include "tree.h"
#include "typecheck.h"

#include <stdio.h>
#include <stdlib.h>

#include <argp.h>

const char *argp_program_version = "parker compiler 0.1";
const char *argp_program_bug_address = "<dryvnt@gmail.com>";

static char doc[] = "A Parker compiler. Not complete.";
static char args_doc[] = "";

static struct argp_option options[] = {
    {"treelog", 'g', 0, OPTION_ARG_OPTIONAL,
     "Print debug info during AST construction", 0},
    {"typechecklog", 't', 0, OPTION_ARG_OPTIONAL,
     "Print debug info during typechecking", 1},
    {"printast", 'a', 0, OPTION_ARG_OPTIONAL, "Print the abstract syntax tree",
     2},
    {"pirlog", 'i', 0, OPTION_ARG_OPTIONAL,
     "Print debug info for PIR creation", 3},
    {"emitfirst", 'f', 0, OPTION_ARG_OPTIONAL,
     "Emit the before it goes through the second pass", 4},
    {"secondlog", 's', 0, OPTION_ARG_OPTIONAL,
     "Print debug info for second pass over ASM (register allocation, mainly)",
     5},
    {0, 0, 0, 0, 0, 0}};

struct arguments {
    bool treelog;
    bool typechecklog;
    bool printast;
    bool pirlog;
    bool emitfirst;
    bool secondlog;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;

    switch (key) {
    case 'g': {
        arguments->treelog = true;
        break;
    }
    case 't': {
        arguments->typechecklog = true;
        break;
    }
    case 'a': {
        arguments->printast = true;
        break;
    }
    case 'i': {
        arguments->pirlog = true;
        break;
    }
    case 'f': {
        arguments->emitfirst = true;
        break;
    }
    case 's': {
        arguments->secondlog = true;
        break;
    }
    case ARGP_KEY_ARG: {
        // if ( state->arg_num >= 0 ) {
        if (true) {
            /* too many arguments */
            fprintf(stderr, "received arg '%s'", arg);
            fprintf(stderr, "Too many arguments\n");
            argp_usage(state);
        }

        break;
    }

    case ARGP_KEY_END: {
        break;
    }

    default: { return ARGP_ERR_UNKNOWN; }
    };
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

int lineno;
int comment_nesting_level;

bool arg_treelog; // icky way to pass arguments to parser
bool arg_typechecklog;
bool arg_pirlog;
bool arg_secondlog;

FUNC *_main = NULL;

void yyparse();

int main(int argc, char **argv) {
    struct arguments arguments = {
        .treelog = false,
        .typechecklog = false,
        .printast = false,
        .pirlog = false,
        .emitfirst = false,
        .secondlog = false,
    };

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    arg_treelog = arguments.treelog; // ew ew ew
    arg_typechecklog = arguments.typechecklog;
    arg_pirlog = arguments.pirlog;
    arg_secondlog = arguments.secondlog;

    lineno = 1;
    comment_nesting_level = 0;

    fprintf(stderr, "----Parsing----\n");
    yyparse();

    if(comment_nesting_level != 0) {
        fprintf(stderr, "Error: Unclosed multi-line comment\n");
        _main = NULL;
    }

    if (_main == NULL) {
        fprintf(stderr, "Scanning/parsing error. Returning failure.\n");
        return EXIT_FAILURE;
    }

    fprintf(stderr, "----Type Checking----\n");

    if (typecheck(_main)) {
        fprintf(stderr, "Typechecking error. Returning failure.\n");
        return EXIT_FAILURE;
    }

    if (arguments.printast) {
        fprintf(stderr, "----Printing AST----\n");
        prettyAST(_main);
    }

    fprintf(stderr, "----Generating Code----\n");
    PIR_function *code_asm = codegen(_main);

    if (arguments.emitfirst) {
        emit(stderr, code_asm);
    }

    fprintf(stderr, "----Second Pass on ASM----\n");
    asm_pass(code_asm);

    fprintf(stderr, "----Emitting----\n");
    emit(stdout, code_asm);

    return 0;
}
