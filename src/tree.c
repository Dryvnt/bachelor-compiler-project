#include "tree.h"
#include "macros.h"
#include "memory.h"

#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern bool arg_treelog;

static void TREE_LOG(const char *fmt, ...) {
    if (arg_treelog) {
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
    }
}

extern int lineno;

NEW_FROM_FUNCTION(VARDECL)
NEW_FROM_FUNCTION(TYPEDECL)
NEW_FROM_FUNCTION(TYPE)
NEW_FROM_FUNCTION(HEAD)
NEW_FROM_FUNCTION(BODY)
NEW_FROM_FUNCTION(TAIL)
NEW_FROM_FUNCTION(FUNC)
NEW_FROM_FUNCTION(DECL)
NEW_FROM_FUNCTION(VAR)
NEW_FROM_FUNCTION(EXP)
NEW_FROM_FUNCTION(STMT)

#define APPEND_LIST_FUNCTION(type)                                  \
    type *append##type##list(type *head, type *follow) {            \
        TREE_LOG("%p: append" #type "list(%p, %p)\n", (void *)head, \
                 (void *)head, (void *)follow);                     \
        if (head == NULL) {                                         \
            return follow;                                          \
        }                                                           \
        type *it = head;                                            \
        while (it->next != NULL) {                                  \
            it = it->next;                                          \
        }                                                           \
        it->next = follow;                                          \
        return head;                                                \
    }

APPEND_LIST_FUNCTION(VARDECL)
APPEND_LIST_FUNCTION(DECL)
APPEND_LIST_FUNCTION(EXP)
APPEND_LIST_FUNCTION(STMT)

VARDECL *makeVARDECL(char *identifier, struct TYPE *type) {
    VARDECL tmp = {.lineno = lineno,
                   .identifier = identifier,
                   .type = type,
                   .next = NULL};

    VARDECL *v = new_VARDECL_from(tmp);
    TREE_LOG("%p: makeVARDECL(\"%s\", %p)\n", (void *)v, identifier,
             (void *)type);
    return v;
}

TYPE *makeTYPEtypename(char *identifier) {
    TYPE tmp = {.kind = typenameK, .val.typenameT = identifier};

    TYPE *t = new_TYPE_from(tmp);
    TREE_LOG("%p: makeTYPEtypename(\"%s\")\n", (void *)t, identifier);
    return t;
}

TYPE *makeTYPEinttype() {
    TYPE tmp = {.kind = inttypeK};

    TYPE *t = new_TYPE_from(tmp);
    TREE_LOG("%p: makeTYPEinttype()\n", (void *)t);
    return t;
}

TYPE *makeTYPEbooltype() {
    TYPE tmp = {.kind = booltypeK};

    TYPE *t = new_TYPE_from(tmp);
    TREE_LOG("%p: makeTYPEbooltype()\n", (void *)t);
    return t;
}

TYPE *makeTYPEarraytype(TYPE *type) {
    TYPE tmp = {.kind = arraytypeK, .val.arraytypeT = type};

    TYPE *t = new_TYPE_from(tmp);
    TREE_LOG("%p: makeTYPEarraytype(%p)\n", (void *)t, (void *)type);
    return t;
}

TYPE *makeTYPErecordtype(VARDECL *members) {
    TYPE tmp = {.kind = recordtypeK, .val.recordtypeT = members};

    TYPE *t = new_TYPE_from(tmp);
    TREE_LOG("%p: makeTYPErecordtype(%p)\n", (void *)t, (void *)members);
    return t;
}

TYPE *makeTYPEimplicit(P_type *type) {
    TYPE tmp = {.kind = implicittypeK, .type_val = type};

    TYPE *t = new_TYPE_from(tmp);
    return t;
}

HEAD *makeHEAD(char *identifier, VARDECL *args, TYPE *returntype) {
    HEAD tmp = {.identifier = identifier,
                .parameters = args,
                .returntype = returntype};

    HEAD *h = new_HEAD_from(tmp);
    TREE_LOG("%p: makeHEAD(\"%s\", %p, %p)\n", (void *)h, identifier,
             (void *)args, (void *)returntype);
    return h;
}

BODY *makeBODY(struct DECL *decls, struct STMT *stmts) {
    BODY tmp = {.declarations = decls, .statements = stmts};

    BODY *b = new_BODY_from(tmp);
    TREE_LOG("%p: makeBODY(%p, %p)\n", (void *)b, (void *)decls,
             (void *)stmts);
    return b;
}

TAIL *makeTAIL(char *identifier) {
    TAIL tmp = {.identifier = identifier};

    TAIL *t = new_TAIL_from(tmp);
    TREE_LOG("%p: makeTAIL(\"%s\")\n", (void *)t, identifier);
    return t;
}

FUNC *makeFUNC(HEAD *head, BODY *body, TAIL *tail) {
    FUNC tmp = {.head = head, .body = body, .tail = tail};

    FUNC *f = new_FUNC_from(tmp);
    TREE_LOG("%p: makeFUNC(%p, %p, %p)\n", (void *)f, (void *)head,
             (void *)body, (void *)tail);
    return f;
}

static TYPEDECL *makeTYPEDECL(char *identifier, TYPE *type) {
    TYPEDECL tmp = {.identifier = identifier, .type = type};

    TYPEDECL *t = new_TYPEDECL_from(tmp);
    return t;
}

DECL *makeDECLtypedecl(char *identifier, TYPE *type) {
    DECL tmp = {.lineno = lineno,
                .kind = typedeclK,
                .val.typedeclD = makeTYPEDECL(identifier, type)};

    DECL *d = new_DECL_from(tmp);
    TREE_LOG("%p: makeDECLtypedecl(\"%s\", %p)\n", (void *)d, identifier,
             (void *)type);
    return d;
}

DECL *makeDECLfuncdecl(FUNC *func) {
    DECL tmp = {.lineno = lineno, .kind = funcdeclK, .val.funcdeclD = func};

    DECL *d = new_DECL_from(tmp);
    TREE_LOG("%p: makeDECLfuncdecl(%p)\n", (void *)d, (void *)func);
    return d;
}

DECL *makeDECLvardecl(VARDECL *vardecl) {
    DECL tmp = {.lineno = lineno, .kind = vardeclK, .val.vardeclD = vardecl};

    DECL *d = new_DECL_from(tmp);
    TREE_LOG("%p: makeDECLvardecl(%p)\n", (void *)d, (void *)vardecl);
    return d;
}

VAR *makeVARident(char *identifier) {
    VAR tmp = {
        .kind = identK, .val.identV = identifier,
    };

    VAR *v = new_VAR_from(tmp);
    TREE_LOG("%p: makeVARident(\"%s\")\n", (void *)v, identifier);
    return v;
}

VAR *makeVARindex(VAR *var, EXP *e) {
    VAR tmp = {.kind = indexK, .val.indexV = {.var = var, .index = e}};

    VAR *v = new_VAR_from(tmp);
    TREE_LOG("%p: makeVARindex(%p, %p)\n", (void *)v, (void *)var, (void *)e);
    return v;
}

VAR *makeVARmember(VAR *var, char *member) {
    VAR tmp = {.kind = memberK, .val.memberV = {.var = var, .member = member}};

    VAR *v = new_VAR_from(tmp);
    TREE_LOG("%p: makeVARmember(%p, \"%s\")\n", (void *)v, (void *)var,
             member);
    return v;
}

// a `op` b
EXP *makeEXPop(const opType op, EXP *a, EXP *b) {
    EXP tmp = {
        .lineno = lineno, .kind = opK, .val.opE = {.op = op, .a = a, .b = b}};

    EXP *e = new_EXP_from(tmp);
    TREE_LOG("%p: makeEXPop(%i, %p, %p)\n", (void *)e, op, (void *)a,
             (void *)b);
    return e;
}

EXP *makeEXPvar(VAR *var) {
    EXP tmp = {.lineno = lineno, .kind = varK, .val.varE = var};

    EXP *e = new_EXP_from(tmp);
    TREE_LOG("%p: makeEXPvar(%p)\n", (void *)e, (void *)var);
    return e;
}

EXP *makeEXPcall(char *identifier, EXP *head) {
    EXP tmp = {.lineno = lineno,
               .kind = callK,
               .val.callE = {.identifier = identifier, .head = head}};

    EXP *e = new_EXP_from(tmp);
    TREE_LOG("%p: makeEXPcall(\"%s\", %p)\n", (void *)e, identifier,
             (void *)e);
    return e;
}

EXP *makeEXPcontainer(const expContainerType t, EXP *inner) {
    EXP tmp = {.lineno = lineno,
               .kind = containerK,
               .val.containerE = {.container = t, .e = inner}};

    EXP *e = new_EXP_from(tmp);
    TREE_LOG("%p: makeEXPcontainer(%i, %p)\n", (void *)e, t, (void *)inner);
    return e;
}

// intconst i
EXP *makeEXPintconst(const int i) {
    EXP tmp = {
        .lineno = lineno, .kind = intconstK, .val.intconstE = i,
    };

    EXP *e = new_EXP_from(tmp);
    TREE_LOG("%p: makeEXPintconst(%i)\n", (void *)e, i);
    return e;
}

EXP *makeEXPboolconst(const bool b) {
    EXP tmp = {.lineno = lineno, .kind = boolconstK, .val.boolconstE = b};

    EXP *e = new_EXP_from(tmp);
    TREE_LOG("%p: makeEXPboolconst(%i)\n", (void *)e, b);
    return e;
}

EXP *makeEXPnull() {
    EXP tmp = {.lineno = lineno, .kind = nullK};

    EXP *e = new_EXP_from(tmp);
    TREE_LOG("%p: makeEXPnull()\n", (void *)e);
    return e;
}

STMT *makeSTMTreturn(EXP *e) {
    STMT tmp = {
        .lineno = lineno, .kind = returnK, .val.returnS = e,
    };

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTreturn(%p)\n", (void *)s, (void *)e);
    return s;
}

STMT *makeSTMTwrite(EXP *e) {
    STMT tmp = {
        .lineno = lineno, .kind = writeK, .val.writeS = e,
    };

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTwrite(%p)\n", (void *)s, (void *)e);
    return s;
}

STMT *makeSTMTallocate(VAR *var) {
    STMT tmp = {.lineno = lineno, .kind = allocK, .val.allocS = var};

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTallocate(%p)\n", (void *)s, (void *)var);
    return s;
}

STMT *makeSTMTallocatelength(VAR *var, EXP *length) {
    STMT tmp = {.lineno = lineno,
                .kind = allocArrayK,
                .val.alloclengthS = {.var = var, .length = length}};

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTallocatelength(%p, %p)\n", (void *)s, (void *)var,
             (void *)length);
    return s;
}

STMT *makeSTMTassignment(VAR *var, EXP *value) {
    STMT tmp = {.lineno = lineno,
                .kind = assignK,
                .val.assignS = {.var = var, .value = value}};

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTassignment(%p, %p)\n", (void *)s, (void *)var,
             (void *)value);
    return s;
}

// if <cond> then <action>
STMT *makeSTMTifthen(EXP *cond, STMT *action) {
    STMT tmp = {.lineno = lineno,
                .kind = ifThenK,
                .val.ifThenS = {.cond = cond, .action = action}};

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTifthen(%p, %p)\n", (void *)s, (void *)cond,
             (void *)action);
    return s;
}

// if <cond> then <action> else <otherwise>
STMT *makeSTMTifthenelse(EXP *cond, STMT *action, STMT *otherwise) {
    STMT tmp = {.lineno = lineno,
                .kind = ifThenElseK,
                .val.ifThenElseS = {
                    .cond = cond, .action = action, .otherwise = otherwise}};

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTifthenelse(%p, %p, %p)\n", (void *)s, (void *)cond,
             (void *)action, (void *)otherwise);
    return s;
}

// while <cond> do <action>
STMT *makeSTMTwhiledo(EXP *cond, STMT *action) {
    STMT tmp = {.lineno = lineno,
                .kind = whileK,
                .val.whileS = {.cond = cond, .action = action}};

    STMT *s = new_STMT_from(tmp);
    TREE_LOG("%p: makeSTMTwhiledo(%p, %p)\n", (void *)s, (void *)cond,
             (void *)action);
    return s;
}
