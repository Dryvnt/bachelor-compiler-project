SOURCEDIR = src
TESTDIR = test

BUILDDIR = build
DEPDIR = $(BUILDDIR)/.d
$(shell mkdir -p $(BUILDDIR) >/dev/null)
$(shell mkdir -p $(DEPDIR) >/dev/null)

SOURCES = $(wildcard $(SOURCEDIR)/*.c)
PARKERSOURCES = $(wildcard $(TESTDIR)/*.src)
OBJECTS =  $(BUILDDIR)/y.tab.o $(BUILDDIR)/lex.yy.o
OBJECTS += $(patsubst $(SOURCEDIR)/%.c, $(BUILDDIR)/%.o, $(SOURCES))

CC = gcc
#-ggdb gives debug info. try the below commands
#valgrind --leak-check=full --track-origin=yes ./build/compiler < [SOURCE]
#gdb -ex 'set args < [SOURCE]' ./build/compiler
DEBUG_CFLAGS = -O0 -ggdb
CFLAGS = -std=c11 -Wall -Wextra -Wpedantic -I$(SOURCEDIR) -I$(BUILDDIR) -fPIC
#CFLAGS += $(DEBUG_CFLAGS)
#-lfl gives default flex routines (for yywrap) (we could not get noyywrap to work)
LDLIBS = -lfl
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.d

COMPILE.c = $(CC) $(DEPFLAGS) $(CFLAGS) -c

.PHONY: test clean

all: $(BUILDDIR)/compiler

#GENERAL
.PHONY: clean
clean:
	rm -rf $(BUILDDIR)

$(BUILDDIR)/%.o : $(SOURCEDIR)/%.c
$(BUILDDIR)/%.o : $(SOURCEDIR)/%.c $(DEPDIR)/%.d
	$(COMPILE.c) -o $@ $<

$(BUILDDIR)/%.o : $(BUILDDIR)/%.c
$(BUILDDIR)/%.o : $(BUILDDIR)/%.c $(DEPDIR)/%.d
	$(COMPILE.c) -o $@ $<

$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

-include $(DEPDIR)/y.tab.d
-include $(DEPDIR)/lex.yy.d
include $(wildcard $(patsubst %,$(DEPDIR)/%.d,$(basename $(SOURCES))))


#SCAN AND PARSE
BISONFLAGS = -Werror -Wall

$(BUILDDIR)/y.tab.c $(BUILDDIR)/y.tab.h: $(SOURCEDIR)/parker.y
	bison $(BISONFLAGS) -o $(BUILDDIR)/y.tab.c -y -d $<

$(BUILDDIR)/lex.yy.c: $(SOURCEDIR)/parker.l $(BUILDDIR)/y.tab.h
	flex -o $(BUILDDIR)/lex.yy.c $<


#PUTTING IT TOGETHER

$(BUILDDIR)/compiler: $(BUILDDIR)/compiler.o $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)

#TESTING
.PHONY: test test_full 
test: test_full symboltest typetest typechecktest

# awkward to list out these tests explicitly like this,
# but the patience for makefiles has run out

symboltest: $(SOURCEDIR)/test/symboltest.c $(BUILDDIR)/memory.o $(BUILDDIR)/hash.o $(BUILDDIR)/symbol.o
	@echo ;\
	echo symboltest:;\
	$(CC) $(CFLAGS) -o $(BUILDDIR)/symboltest $^;\
	./$(BUILDDIR)/symboltest;\

typetest: $(SOURCEDIR)/test/typetest.c $(BUILDDIR)/memory.o $(BUILDDIR)/type.o
	@echo ;\
	echo typetest:;\
	$(CC) $(CFLAGS) -o $(BUILDDIR)/typetest $^;\
	./$(BUILDDIR)/typetest;\

typechecktest: $(SOURCEDIR)/test/typechecktest.c $(BUILDDIR)/memory.o $(BUILDDIR)/typecheck.o $(BUILDDIR)/type.o $(BUILDDIR)/tree.o $(BUILDDIR)/treevisitor.o $(BUILDDIR)/typevisitor.o $(BUILDDIR)/symbol.o $(BUILDDIR)/hash.o
	@echo ;\
	echo typechecktest:;\
	$(CC) $(CFLAGS) -o $(BUILDDIR)/typechecktest $^;\
	./$(BUILDDIR)/typechecktest;\

# end-to-end test of all the testing files
test_full: $(BUILDDIR)/compiler
	cd $(TESTDIR);\
	python2 checkdir.py ../$(BUILDDIR)/compiler .
