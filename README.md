This is the code for my bacherlor project from 2017. I did some minor edits to make it compile on my current machine, but is otherwise provided as-is. It compiles from an imaginary language we called Parker to x86 assembly (GAS), which can be used with gcc to compile to an executable. To try some of the programs for yourself, in this example the Factorial program, do the following commands:
```
make
./compile-and-run ./build/compiler Factorial.src
```

Below is the original readme provided for my delivery.

---

Compiler is compiled on IMADAs machines.

Various debug information can be accessed during compilation. They are activated
by run-time flags. Not all of the debug information is equally useful; Information
useful in an earlier stage of programming is not necessarily useful now. The flags and
a short description of their effect can be found by running the below command.
```
compiler --help
```

Automated tests can be run by using the below command. Each individual test reports its
status (e.g. correct output for end-to-end tests), but the user has to manually sift
through all the output of the command to find out if a test failed.
```
make test
```

If the compiler does not work for what seems to be linking issues or some such,
which should not happen on IMADA machines, it can be recompiled and moved to the root directory by using the below instructions.
```
make clean
make
cp ./build/compiler .
```
